DROP DATABASE apple_music;

CREATE DATABASE apple_music;
USE apple_music;

CREATE TABLE Users
(
    Id          VARCHAR(36) PRIMARY KEY,
    Email       VARCHAR(64) UNIQUE NOT NULL,
    Fullname    VARCHAR(128)       NOT NULL,
    Password    VARCHAR(64)        NOT NULL,
    IsActivated TINYINT(1),
    Created     DATETIME,
    Updated     DATETIME
);

CREATE TABLE UserRoles
(
    Id      VARCHAR(36) PRIMARY KEY,
    UserId  VARCHAR(36),
    Role    VARCHAR(64),
    Created DATETIME,
    Updated DATETIME,
    FOREIGN KEY (UserId) REFERENCES Users (Id) ON DELETE CASCADE
);

CREATE TABLE Categories
(
    Id          VARCHAR(36) PRIMARY KEY,
    Name        VARCHAR(64),
    Image       LONGBLOB NOT NULL,
    IsActive    TINYINT(1),
    Description VARCHAR(128),
    Created     DATETIME,
    Updated     DATETIME
);

CREATE TABLE Courses
(
    Id          VARCHAR(36) PRIMARY KEY,
    Title       VARCHAR(64)    NOT NULL,
    Description VARCHAR(128)   NOT NULL,
    Image       LONGBLOB       NOT NULL,
    IsActive    TINYINT(1),
    Price       DECIMAL(16, 2) NOT NULL,
    Created     DATETIME,
    Updated     DATETIME,
    CategoryId  VARCHAR(36),
    FOREIGN KEY (CategoryId) REFERENCES Categories (Id) ON DELETE CASCADE
);

CREATE TABLE Schedules
(
    Id       VARCHAR(36) PRIMARY KEY,
    Date     DATETIME,
    Created  DATETIME,
    Updated  DATETIME,
    CourseId VARCHAR(36),
    FOREIGN KEY (CourseId) REFERENCES Courses (Id) ON DELETE CASCADE
);

CREATE TABLE Carts
(
    Id         VARCHAR(36) PRIMARY KEY,
    UserId     VARCHAR(36),
    ScheduleId VARCHAR(36),
    Created    DATETIME,
    Updated    DATETIME,
    FOREIGN KEY (UserId) REFERENCES Users (Id) ON DELETE CASCADE,
    FOREIGN KEY (ScheduleId) REFERENCES Schedules (Id) ON DELETE CASCADE
);

CREATE TABLE Payments
(
    Id       VARCHAR(36) PRIMARY KEY,
    Method   VARCHAR(24) NOT NULL,
    Image    LONGBLOB    NOT NULL,
    IsActive TINYINT(1),
    Created  DATETIME,
    Updated  DATETIME
);

CREATE TABLE Orders
(
    Id            VARCHAR(36) PRIMARY KEY,
    UserId        VARCHAR(36),
    PaymentId     VARCHAR(36),
    TotalCourse   INT(8),
    TotalCost     DECIMAL(16, 2),
    PaymentStatus VARCHAR(36),
    Created       DATETIME,
    Updated       DATETIME,
    FOREIGN KEY (PaymentId) REFERENCES Payments (Id) ON DELETE CASCADE,
    FOREIGN KEY (UserId) REFERENCES Users (Id) ON DELETE CASCADE
);

CREATE TABLE OrderDetails
(
    Id         VARCHAR(36) PRIMARY KEY,
    OrderId    VARCHAR(36),
    ScheduleId VARCHAR(36),
    Created    DATETIME,
    Updated    DATETIME,
    FOREIGN KEY (OrderId) REFERENCES Orders (Id) ON DELETE CASCADE,
    FOREIGN KEY (ScheduleId) REFERENCES Schedules (Id) ON DELETE CASCADE
);

CREATE TABLE Invoices
(
    Id             VARCHAR(36) PRIMARY KEY,
    OrderDetailsId VARCHAR(36)  NOT NULL,
    UserId         VARCHAR(36)  NOT NULL,
    NoInvoice      VARCHAR(255) NOT NULL,
    Created        DATETIME     NOT NULL,
    Updated        DATETIME     NOT NULL,
    FOREIGN KEY (OrderDetailsId) REFERENCES OrderDetails (Id) ON DELETE CASCADE,
    FOREIGN KEY (UserId) REFERENCES Users (Id) ON DELETE CASCADE
);

