-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 01, 2023 at 06:24 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET
time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apple_music`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts`
(
    `Id`         varchar(36) NOT NULL,
    `UserId`     varchar(36) DEFAULT NULL,
    `ScheduleId` varchar(36) DEFAULT NULL,
    `Created`    datetime    DEFAULT NULL,
    `Updated`    datetime    DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`Id`, `UserId`, `ScheduleId`, `Created`, `Updated`)
VALUES ('0e8eaec8-d85c-46b0-8686-6fe7b3e3f165', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 00:36:35', '2023-10-01 00:36:35'),
       ('30202d7c-841a-4b97-8b30-cfa391613c60', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 02:42:51', '2023-10-01 02:42:51'),
       ('357eaee6-e9cb-4383-a2e2-a5c42a41f7e5', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 01:11:14', '2023-10-01 01:11:14'),
       ('3a03a03e-4c74-4e45-a43d-dc224e0c2e13', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 02:42:49', '2023-10-01 02:42:49'),
       ('4addbebf-75b9-45e0-9c3b-642c8910cbeb', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 01:11:12', '2023-10-01 01:11:12'),
       ('c82aac4b-78e6-43a6-bed7-65a25de8c2e2', '2c5d97dd-4ba5-430a-be61-18667f876114',
        'd4901db4-c2fc-417b-9d3f-61a15c54832b', '2023-10-01 02:42:58', '2023-10-01 02:42:58');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories`
(
    `Id`          varchar(36)  NOT NULL,
    `Name`        varchar(64)  DEFAULT NULL,
    `Image`       varchar(256) NOT NULL,
    `IsActive`    tinyint(1) DEFAULT NULL,
    `Description` varchar(128) DEFAULT NULL,
    `Created`     datetime     DEFAULT NULL,
    `Updated`     datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`Id`, `Name`, `Image`, `IsActive`, `Description`, `Created`, `Updated`)
VALUES ('639d8c25-37b0-4ff0-aabf-b94f1411c76f', 'Category 1', 'image1.jpg', 1, 'Description 1', '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('c9d242e2-1c67-47f0-954e-0aeaceb38760', 'Category 2', 'image2.jpg', 1, 'Description 2', '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('d35f1934-aaf3-41dd-8634-81fe053a2ddd', 'qwe',
        'D:\\Group6-Batch10-be\\group6-batch10-be\\images\\34a925f9-43d8-4c6e-8877-2de545d3b9a8', 1, 'dsa',
        '2023-09-29 00:00:00', '2023-09-29 21:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses`
(
    `Id`          varchar(36)    NOT NULL,
    `Title`       varchar(64)    NOT NULL,
    `Description` varchar(128)   NOT NULL,
    `Image`       varchar(256)   NOT NULL,
    `IsActive`    tinyint(1) DEFAULT NULL,
    `Price`       decimal(10, 0) NOT NULL,
    `Created`     datetime    DEFAULT NULL,
    `Updated`     datetime    DEFAULT NULL,
    `CategoryId`  varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`Id`, `Title`, `Description`, `Image`, `IsActive`, `Price`, `Created`, `Updated`, `CategoryId`)
VALUES ('57c1584f-e5d4-48c1-90e2-5e63c3c3348e', 'Course 2', 'Course 2 Description', 'course2.jpg', 1, 80,
        '2023-09-29 13:16:15', '2023-09-29 13:16:15', 'c9d242e2-1c67-47f0-954e-0aeaceb38760'),
       ('a7461cbf-6f22-4640-963c-943d47a93992', 'Course 1', 'Course 1 Description', 'course1.jpg', 1, 100,
        '2023-09-29 13:16:15', '2023-09-29 13:16:15', '639d8c25-37b0-4ff0-aabf-b94f1411c76f');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices`
(
    `Id`        varchar(36)  NOT NULL,
        `OrderId`   varchar(36) DEFAULT NULL,
    `UserId`    varchar(36)  NOT NULL,
    `NoInvoice` varchar(255) NOT NULL,
    `Created`   datetime     NOT NULL,
    `Updated`   datetime     NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`Id`, `OrderId`, `UserId`, `NoInvoice`, `Created`, `Updated`)
VALUES ('9a90f08f-caca-4320-837d-4e522e6ac8cb', '428be4c1-e8ad-4667-b46c-73effd2dd35a',
        '2c5d97dd-4ba5-430a-be61-18667f876114', 'APM001', '2023-09-30 19:42:02', '2023-09-30 19:42:02'),
       ('a6d33f1d-bd8d-48c3-9ae4-0a444d646744', '3fcff01b-a5a6-4339-9f94-b0d49068889e',
        '2c5d97dd-4ba5-430a-be61-18667f876114', 'APM002', '2023-09-30 19:43:55', '2023-09-30 19:43:55');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails`
(
    `Id`         varchar(36) NOT NULL,
    `OrderId`    varchar(36) DEFAULT NULL,
    `ScheduleId` varchar(36) DEFAULT NULL,
    `Created`    datetime    DEFAULT NULL,
    `Updated`    datetime    DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`Id`, `OrderId`, `ScheduleId`, `Created`, `Updated`)
VALUES ('f9194a5d-65da-4e8e-89ef-ead24fc054cc', '09dfa49b-15c0-4ae4-85ab-b0eb73a31265',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-09-30 20:21:05', '2023-09-30 20:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders`
(
    `Id`            varchar(36)    NOT NULL,
    `StatusPayment` varchar(36) DEFAULT NULL,
    `TotalCourse`   int(8) NOT NULL,
    `TotalCost`     decimal(16, 0) NOT NULL,
    `Created`       datetime    DEFAULT NULL,
    `Updated`       datetime    DEFAULT NULL,
    `UserId`        varchar(36) DEFAULT NULL,
    `PaymentId`     varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`Id`, `StatusPayment`, `TotalCourse`, `TotalCost`, `Created`, `Updated`, `UserId`, `PaymentId`)
VALUES ('09dfa49b-15c0-4ae4-85ab-b0eb73a31265', 'PAID', 10, 110, '2023-09-30 19:37:37', '2023-09-30 19:37:37',
        '2c5d97dd-4ba5-430a-be61-18667f876114', '5d7e5bde-3999-49c9-84ac-8df80da7e0d7'),
       ('3fcff01b-a5a6-4339-9f94-b0d49068889e', 'PAID', 10, 1110, '2023-09-30 19:43:55', '2023-09-30 19:43:55',
        '2c5d97dd-4ba5-430a-be61-18667f876114', '5d7e5bde-3999-49c9-84ac-8df80da7e0d7'),
       ('428be4c1-e8ad-4667-b46c-73effd2dd35a', 'PAID', 10, 1110, '2023-09-30 19:42:02', '2023-09-30 19:42:02',
        '2c5d97dd-4ba5-430a-be61-18667f876114', '5d7e5bde-3999-49c9-84ac-8df80da7e0d7');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments`
(
    `Id`      varchar(36)  NOT NULL,
    `Method`  varchar(24)  NOT NULL,
    `Image`   varchar(128) NOT NULL,
    `Created` datetime DEFAULT NULL,
    `Updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`Id`, `Method`, `Image`, `Created`, `Updated`)
VALUES ('5d7e5bde-3999-49c9-84ac-8df80da7e0d7', 'PayPal', 'payment2.jpg', '2023-09-29 13:16:15', '2023-09-29 13:16:15'),
       ('b1693ab5-c105-4910-a76b-dc8a1390d9b4', 'Credit Card', 'payment1.jpg', '2023-09-29 13:16:15',
        '2023-09-29 13:16:15');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules`
(
    `Id`       varchar(36) NOT NULL,
    `CourseId` varchar(36) NOT NULL,
    `Date`     datetime DEFAULT NULL,
    `Created`  datetime DEFAULT NULL,
    `Updated`  datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`Id`, `CourseId`, `Date`, `Created`, `Updated`)
VALUES ('8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', 'a7461cbf-6f22-4640-963c-943d47a93992', '2023-09-28 10:00:00',
        '2023-09-29 13:36:11', '2023-09-29 13:36:11'),
       ('d4901db4-c2fc-417b-9d3f-61a15c54832b', 'a7461cbf-6f22-4640-963c-943d47a93992', '2023-09-29 14:00:00',
        '2023-09-29 13:36:11', '2023-09-29 13:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles`
(
    `Id`      varchar(36) NOT NULL,
    `UserId`  varchar(36) DEFAULT NULL,
    `Role`    tinyint(1) DEFAULT NULL,
    `Created` datetime    DEFAULT NULL,
    `Updated` datetime    DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`Id`, `UserId`, `Role`, `Created`, `Updated`)
VALUES ('2c5d97dd-4ba5-430a-be61-18667f876114', '2c5d97dd-4ba5-430a-be61-18667f876114', 1, '2023-10-01 00:00:00',
        '2023-10-01 00:00:00'),
       ('6ac4b6b0-79da-4db5-835e-5a45f85ebcfb', '7c67444f-8e3a-4e7a-9bd6-d33a04c6e8b2', 1, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('ad2d6b0d-5d58-48ea-a5da-c6286b3e6f51', 'ad2d6b0d-5d58-48ea-a5da-c6286b3e6f51', 1, '2023-10-01 00:00:00',
        '2023-10-01 00:00:00'),
       ('c15e377b-d3a0-4b08-b303-0eab1f54ff4d', 'd7a3bfc9-857e-4e52-9142-91c197aacb2e', 0, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users`
(
    `Id`          varchar(36)  NOT NULL,
    `Email`       varchar(64)  NOT NULL,
    `Fullname`    varchar(128) NOT NULL,
    `Password`    varchar(64)  NOT NULL,
    `isActivated` tinyint(1) DEFAULT NULL,
    `Created`     datetime DEFAULT NULL,
    `Updated`     datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `Email`, `Fullname`, `Password`, `isActivated`, `Created`, `Updated`)
VALUES ('2c5d97dd-4ba5-430a-be61-18667f876114', 'nandahafidz24@gmail.com', 'Dewa',
        '$2a$11$UIYSHtsqxrfBr3k3k6Tc/O4dQcFa5CkDxg9NpZCozbvFSji..OBce', 1, '2023-10-01 00:00:00',
        '2023-10-01 10:38:28'),
       ('7c67444f-8e3a-4e7a-9bd6-d33a04c6e8b2', 'user1@example.com', 'User 1', 'password1', 1, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('ad2d6b0d-5d58-48ea-a5da-c6286b3e6f51', 'string@gmail.com', 'string',
        '$2a$11$OC/pAdHBta8SzfjqBp/cmu2Y.TI8swGTvh6M8a3AfeIXAhFAGIPqi', 1, '2023-10-01 00:00:00',
        '2023-10-01 00:00:00'),
       ('d7a3bfc9-857e-4e52-9142-91c197aacb2e', 'user2@example.com', 'User 2', 'string', 1, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Carts_UserId` (`UserId`),
  ADD KEY `FK_Carts_ScheduleId` (`ScheduleId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
    ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Courses_CategoryId` (`CategoryId`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Invoices_OrderId` (`OrderId`),
  ADD KEY `FK_Invoices_UserId` (`UserId`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_OrderDetails_OrderId` (`OrderId`),
  ADD KEY `FK_OrderDetails_ScheduleId` (`ScheduleId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `PaymentId` (`PaymentId`),
  ADD KEY `FK_Orders_UserId` (`UserId`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
    ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Schedules_CourseId` (`CourseId`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
    ADD CONSTRAINT `FK_Carts_ScheduleId` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Carts_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`),
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
    ADD CONSTRAINT `FK_Courses_CategoryId` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`Id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
    ADD CONSTRAINT `FK_Invoices_OrderId` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Invoices_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE
CASCADE,
  ADD CONSTRAINT `invoices_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE
CASCADE;

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
    ADD CONSTRAINT `FK_OrderDetails_OrderId` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_OrderDetails_ScheduleId` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE
CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
    ADD CONSTRAINT `FK_Orders_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`PaymentId`) REFERENCES `payments` (`Id`) ON DELETE
CASCADE;

--
-- Constraints for table `schedules`
--
ALTER TABLE `schedules`
    ADD CONSTRAINT `FK_Schedules_CourseId` FOREIGN KEY (`CourseId`) REFERENCES `courses` (`Id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_Schedules_Courses` FOREIGN KEY (`CourseId`) REFERENCES `courses` (`Id`);

--
-- Constraints for table `userroles`
--
ALTER TABLE `userroles`
    ADD CONSTRAINT `userroles_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
