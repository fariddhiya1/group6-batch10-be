
![Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Apple_Music_logo.svg/320px-Apple_Music_logo.svg.png)


# Apple Music

An online course on music as a learning project at CodingID.


## Documentation

[Design Database](https://drive.google.com/file/d/1ErQ_BJxD3CTytlRJqYH698cPE_WFJ14J/view?usp=sharing)


## Tech Stack

**Client:** React, MaterialUI, TailwindCSS

**Server:** .NET, .NET Core, ADO.NET

**Database:** MySQL

**Cloud:** DigitalOcean (on progress)

## Features

- Login & Register
- Choose Course, Order, Invoice
- Responsive Website


## Attachment

- [Link Gitlab Frontend](https://gitlab.com/Lawnd/group6-batch10)
- [Link Gitlab Backend](https://gitlab.com/fariddhiya1/group6-batch10-be)


## Authors

- [@nandahafidz](https://www.linkedin.com/in/nandahafidz24/)
- [@fariddhiya](https://www.linkedin.com/in/farid-arif/)


