-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 01, 2023 at 06:23 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET
time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apple_music`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts`
(
    `Id`         varchar(36) NOT NULL,
    `UserId`     varchar(36) DEFAULT NULL,
    `ScheduleId` varchar(36) DEFAULT NULL,
    `Created`    datetime    DEFAULT NULL,
    `Updated`    datetime    DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories`
(
    `Id`          varchar(36)  NOT NULL,
    `Name`        varchar(64)  DEFAULT NULL,
    `Image`       varchar(256) NOT NULL,
    `IsActive`    tinyint(1) DEFAULT NULL,
    `Description` varchar(128) DEFAULT NULL,
    `Created`     datetime     DEFAULT NULL,
    `Updated`     datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses`
(
    `Id`          varchar(36)    NOT NULL,
    `Title`       varchar(64)    NOT NULL,
    `Description` varchar(128)   NOT NULL,
    `Image`       varchar(256)   NOT NULL,
    `IsActive`    tinyint(1) DEFAULT NULL,
    `Price`       decimal(10, 0) NOT NULL,
    `Created`     datetime    DEFAULT NULL,
    `Updated`     datetime    DEFAULT NULL,
    `CategoryId`  varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices`
(
    `Id`        varchar(36)  NOT NULL,
    `OrderId`   varchar(36) DEFAULT NULL,
    `UserId`    varchar(36)  NOT NULL,
    `NoInvoice` varchar(255) NOT NULL,
    `Created`   datetime     NOT NULL,
    `Updated`   datetime     NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails`
(
    `Id`         varchar(36) NOT NULL,
    `OrderId`    varchar(36) DEFAULT NULL,
    `ScheduleId` varchar(36) DEFAULT NULL,
    `Created`    datetime    DEFAULT NULL,
    `Updated`    datetime    DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders`
(
    `Id`            varchar(36)    NOT NULL,
    `StatusPayment` varchar(36) DEFAULT NULL,
    `TotalCourse`   int(8) NOT NULL,
    `TotalCost`     decimal(16, 0) NOT NULL,
    `Created`       datetime    DEFAULT NULL,
    `Updated`       datetime    DEFAULT NULL,
    `UserId`        varchar(36) DEFAULT NULL,
    `PaymentId`     varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments`
(
    `Id`       varchar(36)  NOT NULL,
    `Method`   varchar(24)  NOT NULL,
    `Image`    varchar(128) NOT NULL,
    `IsActive` tinyint(1) NOT NULL,
    `Created`  datetime DEFAULT NULL,
    `Updated`  datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules`
(
    `Id`       varchar(36) NOT NULL,
    `CourseId` varchar(36) NOT NULL,
    `Date`     datetime DEFAULT NULL,
    `Created`  datetime DEFAULT NULL,
    `Updated`  datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles`
(
    `Id`      varchar(36) NOT NULL,
    `UserId`  varchar(36) DEFAULT NULL,
    `Role`    tinyint(1) DEFAULT NULL,
    `Created` datetime    DEFAULT NULL,
    `Updated` datetime    DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users`
(
    `Id`          varchar(36)  NOT NULL,
    `Email`       varchar(64)  NOT NULL,
    `Fullname`    varchar(128) NOT NULL,
    `Password`    varchar(64)  NOT NULL,
    `isActivated` tinyint(1) DEFAULT NULL,
    `Created`     datetime DEFAULT NULL,
    `Updated`     datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Carts_UserId` (`UserId`),
  ADD KEY `FK_Carts_ScheduleId` (`ScheduleId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
    ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Courses_CategoryId` (`CategoryId`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Invoices_OrderId` (`OrderId`),
  ADD KEY `FK_Invoices_UserId` (`UserId`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_OrderDetails_OrderId` (`OrderId`),
  ADD KEY `FK_OrderDetails_ScheduleId` (`ScheduleId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `PaymentId` (`PaymentId`),
  ADD KEY `FK_Orders_UserId` (`UserId`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
    ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Schedules_CourseId` (`CourseId`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
    ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
    ADD CONSTRAINT `FK_Carts_ScheduleId` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Carts_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`),
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
    ADD CONSTRAINT `FK_Courses_CategoryId` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`Id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
    ADD CONSTRAINT `FK_Invoices_OrderId` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Invoices_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE
CASCADE,
  ADD CONSTRAINT `invoices_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE
CASCADE;

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
    ADD CONSTRAINT `FK_OrderDetails_OrderId` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_OrderDetails_ScheduleId` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE
CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`ScheduleId`) REFERENCES `schedules` (`Id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
    ADD CONSTRAINT `FK_Orders_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON
DELETE
CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`PaymentId`) REFERENCES `payments` (`Id`) ON DELETE
CASCADE;

--
-- Constraints for table `schedules`
--
ALTER TABLE `schedules`
    ADD CONSTRAINT `FK_Schedules_CourseId` FOREIGN KEY (`CourseId`) REFERENCES `courses` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Schedules_Courses` FOREIGN KEY (`CourseId`) REFERENCES `courses` (`Id`);

--
-- Constraints for table `userroles`
--
ALTER TABLE `userroles`
    ADD CONSTRAINT `userroles_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
