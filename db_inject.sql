INSERT INTO `Users` (`Id`, `Email`, `Fullname`, `Password`, `isActivated`, `Created`, `Updated`)
VALUES ('2c5d97dd-4ba5-430a-be61-18667f876114', 'nandahafidz24@gmail.com', 'Dewa',
        '$2a$11$UIYSHtsqxrfBr3k3k6Tc/O4dQcFa5CkDxg9NpZCozbvFSji..OBce', 1, '2023-10-01 00:00:00',
        '2023-10-01 10:38:28'),
       ('7c67444f-8e3a-4e7a-9bd6-d33a04c6e8b2', 'user1@example.com', 'User 1', 'password1', 1, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('ad2d6b0d-5d58-48ea-a5da-c6286b3e6f51', 'string@gmail.com', 'string',
        '$2a$11$OC/pAdHBta8SzfjqBp/cmu2Y.TI8swGTvh6M8a3AfeIXAhFAGIPqi', 1, '2023-10-01 00:00:00',
        '2023-10-01 00:00:00'),
       ('d7a3bfc9-857e-4e52-9142-91c197aacb2e', 'user2@example.com', 'User 2', 'string', 1, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15');

INSERT INTO `UserRoles` (`Id`, `UserId`, `Role`, `Created`, `Updated`)
VALUES ('2c5d97dd-4ba5-430a-be61-18667f876114', '2c5d97dd-4ba5-430a-be61-18667f876114', 1, '2023-10-01 00:00:00',
        '2023-10-01 00:00:00'),
       ('6ac4b6b0-79da-4db5-835e-5a45f85ebcfb', '7c67444f-8e3a-4e7a-9bd6-d33a04c6e8b2', 1, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('ad2d6b0d-5d58-48ea-a5da-c6286b3e6f51', 'ad2d6b0d-5d58-48ea-a5da-c6286b3e6f51', 1, '2023-10-01 00:00:00',
        '2023-10-01 00:00:00'),
       ('c15e377b-d3a0-4b08-b303-0eab1f54ff4d', 'd7a3bfc9-857e-4e52-9142-91c197aacb2e', 0, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15');

INSERT INTO `Categories` (`Id`, `Name`, `Image`, `IsActive`, `Description`, `Created`, `Updated`)
VALUES ('639d8c25-37b0-4ff0-aabf-b94f1411c76f', 'Category 1', 'image1.jpg', 1, 'Description 1', '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('c9d242e2-1c67-47f0-954e-0aeaceb38760', 'Category 2', 'image2.jpg', 1, 'Description 2', '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('d35f1934-aaf3-41dd-8634-81fe053a2ddd', 'qwe',
        'D:\\Group6-Batch10-be\\group6-batch10-be\\images\\34a925f9-43d8-4c6e-8877-2de545d3b9a8', 1, 'dsa',
        '2023-09-29 00:00:00', '2023-09-29 21:30:59');

INSERT INTO `Courses` (`Id`, `Title`, `Description`, `Image`, `IsActive`, `Price`, `Created`, `Updated`, `CategoryId`)
VALUES ('57c1584f-e5d4-48c1-90e2-5e63c3c3348e', 'Course 2', 'Course 2 Description', 'course2.jpg', 1, 80,
        '2023-09-29 13:16:15', '2023-09-29 13:16:15', 'c9d242e2-1c67-47f0-954e-0aeaceb38760'),
       ('a7461cbf-6f22-4640-963c-943d47a93992', 'Course 1', 'Course 1 Description', 'course1.jpg', 1, 100,
        '2023-09-29 13:16:15', '2023-09-29 13:16:15', '639d8c25-37b0-4ff0-aabf-b94f1411c76f');

INSERT INTO `Schedules` (`Id`, `CourseId`, `Date`, `Created`, `Updated`)
VALUES ('8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', 'a7461cbf-6f22-4640-963c-943d47a93992', '2023-09-28 10:00:00',
        '2023-09-29 13:36:11', '2023-09-29 13:36:11'),
       ('d4901db4-c2fc-417b-9d3f-61a15c54832b', 'a7461cbf-6f22-4640-963c-943d47a93992', '2023-09-29 14:00:00',
        '2023-09-29 13:36:11', '2023-09-29 13:36:11');

INSERT INTO `Carts` (`Id`, `UserId`, `ScheduleId`, `Created`, `Updated`)
VALUES ('0e8eaec8-d85c-46b0-8686-6fe7b3e3f165', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 00:36:35', '2023-10-01 00:36:35'),
       ('30202d7c-841a-4b97-8b30-cfa391613c60', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 02:42:51', '2023-10-01 02:42:51'),
       ('357eaee6-e9cb-4383-a2e2-a5c42a41f7e5', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 01:11:14', '2023-10-01 01:11:14'),
       ('3a03a03e-4c74-4e45-a43d-dc224e0c2e13', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 02:42:49', '2023-10-01 02:42:49'),
       ('4addbebf-75b9-45e0-9c3b-642c8910cbeb', '2c5d97dd-4ba5-430a-be61-18667f876114',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-10-01 01:11:12', '2023-10-01 01:11:12'),
       ('c82aac4b-78e6-43a6-bed7-65a25de8c2e2', '2c5d97dd-4ba5-430a-be61-18667f876114',
        'd4901db4-c2fc-417b-9d3f-61a15c54832b', '2023-10-01 02:42:58', '2023-10-01 02:42:58');

INSERT INTO `Payments` (`Id`, `Method`, `Image`, `IsActive`, `Created`, `Updated`)
VALUES ('5d7e5bde-3999-49c9-84ac-8df80da7e0d7', 'PayPal', 'payment2.jpg', true, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15'),
       ('b1693ab5-c105-4910-a76b-dc8a1390d9b4', 'Credit Card', 'payment1.jpg', false, '2023-09-29 13:16:15',
        '2023-09-29 13:16:15');

INSERT INTO `Orders` (`Id`, `PaymentStatus`, `TotalCourse`, `TotalCost`, `Created`, `Updated`, `UserId`, `PaymentId`)
VALUES ('09dfa49b-15c0-4ae4-85ab-b0eb73a31265', 'PAID', 10, 110, '2023-09-30 19:37:37', '2023-09-30 19:37:37',
        '2c5d97dd-4ba5-430a-be61-18667f876114', '5d7e5bde-3999-49c9-84ac-8df80da7e0d7'),
       ('3fcff01b-a5a6-4339-9f94-b0d49068889e', 'PAID', 10, 1110, '2023-09-30 19:43:55', '2023-09-30 19:43:55',
        '2c5d97dd-4ba5-430a-be61-18667f876114', '5d7e5bde-3999-49c9-84ac-8df80da7e0d7'),
       ('428be4c1-e8ad-4667-b46c-73effd2dd35a', 'PAID', 10, 1110, '2023-09-30 19:42:02', '2023-09-30 19:42:02',
        '2c5d97dd-4ba5-430a-be61-18667f876114', '5d7e5bde-3999-49c9-84ac-8df80da7e0d7');

INSERT INTO `OrderDetails` (`Id`, `OrderId`, `ScheduleId`, `Created`, `Updated`)
VALUES ('f9194a5d-65da-4e8e-89ef-ead24fc054cc', '09dfa49b-15c0-4ae4-85ab-b0eb73a31265',
        '8a2a5a94-ebf2-4b22-bfd5-1e1ee692997e', '2023-09-30 20:21:05', '2023-09-30 20:21:05');

INSERT INTO `Invoices` (`Id`, `OrderDetailsId`, `UserId`, `NoInvoice`, `Created`, `Updated`)
VALUES ('9a90f08f-caca-4320-837d-4e522e6ac8cb', 'f9194a5d-65da-4e8e-89ef-ead24fc054cc',
        '2c5d97dd-4ba5-430a-be61-18667f876114', 'APM001', '2023-09-30 19:42:02', '2023-09-30 19:42:02'),
       ('a6d33f1d-bd8d-48c3-9ae4-0a444d646744', 'f9194a5d-65da-4e8e-89ef-ead24fc054cc',
        '2c5d97dd-4ba5-430a-be61-18667f876114', 'APM002', '2023-09-30 19:43:55', '2023-09-30 19:43:55');













