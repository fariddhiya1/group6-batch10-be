namespace group6_batch10_be.Helpers;

public class Helper
{
  public static string FormatDate(DateTime dateTime)
  {
    return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
  }
  
  public static string SaveImage(IFormFile imageFile)
  {
    try
    {
      Guid fileName = Guid.NewGuid();
      string directoryPath = Path.Combine(Directory.GetCurrentDirectory(), "images");
      Directory.CreateDirectory(directoryPath);

      string imagePath = Path.Combine(directoryPath, fileName.ToString());

      using (Stream stream = new FileStream(imagePath, FileMode.Create))
      {
        imageFile.CopyTo(stream);
      }

      return imagePath;
    }
    catch (Exception ex)
    {
      return null;
    }
  }
  
  public static byte[] GetPhoto(string filePath)  
  {  
    FileStream stream = new FileStream(  
      filePath, FileMode.Open, FileAccess.Read);  
    BinaryReader reader = new BinaryReader(stream);  
  
    byte[] photo = reader.ReadBytes((int)stream.Length);  
  
    reader.Close();  
    stream.Close();  
  
    return photo;  
  }  
  
    public static byte[] GetImageBytes(IFormFile imageFile)
    {
      using (MemoryStream ms = new MemoryStream())
      {
        // Membaca file gambar dan menyalinnya ke MemoryStream
        imageFile.CopyTo(ms);
        return ms.ToArray(); // Mengambil byte array dari MemoryStream
      }
    }
}