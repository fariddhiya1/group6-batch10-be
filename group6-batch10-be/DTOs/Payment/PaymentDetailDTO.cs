namespace group6_batch10_be.DTOs.Payment;

public class PaymentDetailDTO
{
  public Guid Id { get; set; }
  public string Method { get; set; } = string.Empty;
  public bool? IsActive { get; set; }
  public string? Image { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}