namespace group6_batch10_be.DTOs.Payment;

public class PaymentDTO
{
    public string? Method { get; set; } 
    public bool? IsActive { get; set; }
    public IFormFile? ImageFile { get; set; }
}