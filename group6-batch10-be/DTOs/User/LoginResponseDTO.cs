﻿namespace group6_batch10_be.DTOs.User;

public class LoginResponseDTO
{
  public string Token { get; set; } = string.Empty;
  public string Message { get; set; } = string.Empty;
}