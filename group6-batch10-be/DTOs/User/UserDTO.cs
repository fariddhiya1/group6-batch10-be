﻿namespace group6_batch10_be.DTOs.User;

public class UserDTO
{
    public Guid Id { get; set; }
    public string Fullname { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public bool Role { get; set; } = false;
    public string Email { get; set; } = string.Empty;
    public bool? IsActivated { get; set; }

}
