﻿namespace group6_batch10_be.DTOs.Order;

public class OrderDTO
{
  public int TotalCourse { get; set; }
  public decimal TotalCost { get; set; }
  public Guid PaymentId { get; set; }

}