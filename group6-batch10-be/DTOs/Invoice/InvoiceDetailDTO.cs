﻿namespace group6_batch10_be.DTOs.Invoice;

public class InvoiceDetailDTO
{
  public Guid InvoiceID { get; set; }
  public Guid UserId { get; set; }
  public string Email { get; set; }
  public string NoInvoice { get; set; }
  public string NameCourse { get; set; }
  public decimal TotalCost { get; set; }
  public string CategoryCourse { get; set; }
  public string Schedule { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}