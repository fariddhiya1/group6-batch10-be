﻿namespace group6_batch10_be.DTOs.Invoice;

public class AllOrderInvoiceDTO
{
  public Guid OrderId { get; set; }
  public string Email { get; set; } = string.Empty;
  public string NoInvoice { get; set; }
  public int TotalCourse { get; set; }
  public decimal TotalCost { get; set; }
  public DateTime Created { get; set; }
}