namespace group6_batch10_be.DTOs.Invoice;

public class InvoiceDTO
{
  public Guid OrderId { get; set; }
  public string NoInvoice { get; set; }
  public int TotalCourse { get; set; }
  public decimal TotalCost { get; set; }
  public DateTime Created { get; set; }
}