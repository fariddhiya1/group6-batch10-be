﻿namespace group6_batch10_be.DTOs.Invoice;

public class DetailInvoiceDTO
{
  public string Title { get; set; } = string.Empty;
  public string Category { get; set; } = string.Empty;
  public DateTime Date { get; set; }
  public decimal Price { get; set; }
}