﻿namespace group6_batch10_be.DTOs.Cart;

public class CartViewDTO
{
  public Guid Id { get; set; }
  public Guid ScheduleId { get; set; }
  public string Category { get; set; } = string.Empty;
  public string Title { get; set; } = string.Empty;
  public DateTime Date { get; set; }
  public decimal Price { get; set; }
  public string Image { get; set; } = string.Empty;
}