﻿namespace group6_batch10_be.DTOs.OrderDetail;

public class OrderDetailDTO
{
  public Guid OrderId { get; set; }
  public Guid ScheduleId { get; set; }
  public String LastInvoiceNumber { get; set; }
}