﻿namespace group6_batch10_be.DTOs.OrderDetail;

public class MyClassDTO
{
  public string Category { get; set; } = string.Empty;
  public string Title { get; set; } = string.Empty;
  public DateTime Date { get; set; }
  public string Image { get; set; } = string.Empty;
}