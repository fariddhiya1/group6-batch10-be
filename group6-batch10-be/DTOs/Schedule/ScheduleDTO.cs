namespace group6_batch10_be.DTOs;

public class ScheduleDTO
{
  public DateTime Date { get; set; }
  public Guid CourseId { get; set; }
}