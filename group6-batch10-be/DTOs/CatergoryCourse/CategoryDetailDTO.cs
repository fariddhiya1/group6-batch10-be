namespace group6_batch10_be.DTOs.CatergoryCourse;

public class CategoryDetailDTO
{
  public Guid Id { get; set; }
  public string Name { get; set; } = string.Empty;
  public string? Description { get; set; }
  public bool? IsActive { get; set; }
  public string? Image { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}