﻿namespace group6_batch10_be.DTOs.CatergoryCourse;

public class CategoryCourseDTO
{
  public string Name { get; set; } = string.Empty;
  public string? Description { get; set; }
  public bool? IsActive { get; set; }
  public IFormFile? ImageFile { get; set; }
}