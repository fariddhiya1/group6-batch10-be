namespace group6_batch10_be.DTOs;

public class CourseDetailDTO
{
  public Guid Id { get; set; }
  public string Title { get; set; } = String.Empty;
  public string Description { get; set; } = String.Empty;
  public string? Image { get; set; }
  public decimal Price { get; set; }
  public bool IsActive { get; set; }
  public String? Category { get; set; } = String.Empty;
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}