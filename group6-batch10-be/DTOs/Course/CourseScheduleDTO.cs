namespace group6_batch10_be.DTOs;

public class CourseScheduleDTO
{
  public Guid Id { get; set; }
  public string Title { get; set; }
  public string Category { get; set; }
  public decimal Price { get; set; }
  public string Image { get; set; }
  public bool IsActive { get; set; }
  public List<ScheduleList> ListSchedule { get; set; }
}

public class ScheduleList
{
  public Guid Id { get; set; }
  public DateTime Date { get; set; }
}