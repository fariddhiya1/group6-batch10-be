namespace group6_batch10_be.DTOs;

public class CourseDto
{
  public string? Title { get; set; }
  public string? Description { get; set; }
  public IFormFile? ImageFile { get; set; }
  public decimal? Price { get; set; }
  public bool? IsActive { get; set; }
  public Guid? CategoryId { get; set; }
}