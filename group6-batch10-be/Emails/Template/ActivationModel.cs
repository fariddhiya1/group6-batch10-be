namespace group6_batch10_be.Emails.Template;

public class ActivationModel
{
  public string? Email { get; set; }
  public string? Link { get; set; }
}
