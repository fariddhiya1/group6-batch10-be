﻿using group6_batch10_be.DTOs.Cart;
using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class CartDataAccess
{
  private readonly string _connectionString;
  private readonly IConfiguration _configuration;

  public CartDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _connectionString = _configuration.GetConnectionString("DefaultConnection");
  }

  public bool AddCart(Cart cart)
  {
    bool result = false;

    string created = Helper.FormatDate(cart.Created);
    string updated = Helper.FormatDate(cart.Updated);

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.Parameters.Clear();

        command.CommandText = "INSERT INTO Carts (Id, UserId, ScheduleId, Created, Updated) " +
                              "VALUES (@id, @userid, @scheduleid, @created, @updated);";

        command.Parameters.AddWithValue("@id", cart.Id);
        command.Parameters.AddWithValue("@userid", cart.UserId);
        command.Parameters.AddWithValue("@scheduleid", cart.ScheduleId);
        command.Parameters.AddWithValue("@created", created);
        command.Parameters.AddWithValue("@updated", updated);

        try
        {
          connection.Open();

          int execresult = command.ExecuteNonQuery();

          result = execresult > 0 ? true : false;
        }
        catch
        {
          throw;
        }
        finally
        {
          connection.Close();
        }
      }
    }

    return result;
  }

  public List<CartViewDTO> GetViewCart(string userId)
  {
    List<CartViewDTO> carts = new List<CartViewDTO>();

    string query =
       "SELECT c.Id AS Id_Cart, s.Id AS Id_Schedule, t.Name AS Type_Name, m.Title, s.Date, m.Price, m.Image " +
       "FROM `Schedules` s " +
       "INNER JOIN `Carts` c " +
       "ON s.Id = c.ScheduleId " +
       "INNER JOIN `Courses` m " +
       "ON s.CourseId = m.Id " +
       "INNER JOIN `Categories` t " +
       "ON m.CategoryId = t.Id " +
       "WHERE c.UserId = @userId " +
       "ORDER BY c.Created DESC;"; 



        using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Connection = connection;
        command.Parameters.Clear();


        command.Parameters.AddWithValue("@userId", userId);

        try
        {
          connection.Open();

          using (MySqlDataReader reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              byte[] imageBytes = (byte[])reader["Image"];
              string base64Image = Convert.ToBase64String(imageBytes);
              carts.Add(new CartViewDTO
              {
                Id = Guid.Parse(reader["Id_Cart"].ToString() ?? string.Empty),
                ScheduleId = Guid.Parse(reader["Id_Schedule"].ToString() ?? string.Empty),
                Category = reader["Type_Name"].ToString() ?? string.Empty,
                Title = reader["Title"].ToString() ?? string.Empty,
                Date = Convert.ToDateTime(reader["Date"]),
                Price = Convert.ToDecimal(reader["Price"]),
                Image = base64Image,
              });
            }
          }
        }
        catch
        {
          throw;
        }
        finally
        {
          connection.Close();
        }
      }
    }

    return carts;
  }


  public bool DeleteCart(Guid Id)
  {
    bool result = false;

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.Parameters.Clear();


        command.CommandText = "DELETE FROM Carts WHERE Id = @Id;";


        command.Parameters.AddWithValue("@Id", Id);

        try
        {
          connection.Open();

          int execresult = command.ExecuteNonQuery();

          result = execresult > 0 ? true : false;
        }
        catch
        {
          throw;
        }
        finally
        {
          connection.Close();
        }
      }
    }

    return result;
  }
}