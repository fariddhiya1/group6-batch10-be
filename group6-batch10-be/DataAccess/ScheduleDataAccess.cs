using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class ScheduleDataAccess
{
  private readonly string _conectionString;
  private readonly IConfiguration _configuration;

  public ScheduleDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _conectionString = _configuration.GetConnectionString("DefaultConnection");
  }

  public List<Schedule> GetAll()
  {
    List<Schedule> schedules = new List<Schedule>();
    string query =
      "SELECT * FROM Schedules";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        connection.Open();

        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            schedules.Add(new Schedule
            {
              Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
              Date = Convert.ToDateTime(reader["Date"]),
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"]),
              CourseId = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty)
            });
          }
        }

        connection.Close();
      }
    }

    return schedules;
  }

  public Schedule? GetById(Guid id)
  {
    Schedule? schedule = null;

    string query = "SELECT * FROM Schedules WHERE Id = @Id";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", id);

        connection.Open();
        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            schedule = new Schedule
            {
              Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
              Date = Convert.ToDateTime(reader["Date"]),
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"]),
              CourseId = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty),
            };
          }
        }

        connection.Close();
      }
    }

    return schedule;
  }

  public bool Insert(Schedule schedule)
  {
    bool result = false;

    string date = Helper.FormatDate(schedule.Date);
    string created = Helper.FormatDate(schedule.Created);
    string updated = Helper.FormatDate(schedule.Updated);

    string query = "INSERT INTO Schedules (Id, Date, Created, Updated, CourseId) " +
                   "VALUES (@Id, @Date, @Created, @Updated, @CourseId)";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", schedule.Id);
        command.Parameters.AddWithValue("@Date", date);
        command.Parameters.AddWithValue("@Created", created);
        command.Parameters.AddWithValue("@Updated", updated);
        command.Parameters.AddWithValue("@CourseId", schedule.CourseId);

        connection.Open();
        result = command.ExecuteNonQuery() > 0 ? true : false;
        connection.Close();
      }
    }

    return result;
  }

  public bool Update(Guid id, Schedule schedule)
  {
    bool result = false;

    string updated = Helper.FormatDate(schedule.Updated);
    string date = Helper.FormatDate(schedule.Date);

    string query =
      $"UPDATE Schedules SET Date = @Date, Updated = @Updated WHERE Id = @Id";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", id);
        command.Parameters.AddWithValue("@Date", date);
        command.Parameters.AddWithValue("@Updated", updated);

        connection.Open();
        result = command.ExecuteNonQuery() > 0 ? true : false;
        connection.Close();
      }
    }

    return result;
  }

  public bool Delete(Guid id)
  {
    bool result = false;

    string query = $"DELETE FROM Schedules WHERE Id = '{id}'";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        connection.Open();
        result = command.ExecuteNonQuery() > 0 ? true : false;
        connection.Close();
      }
    }

    return result;
  }
}