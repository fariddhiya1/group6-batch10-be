﻿using group6_batch10_be.DTOs.Order;
using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class OrderDataAccess
{
  private readonly string _connectionString;
  private readonly IConfiguration _configuration;

  public OrderDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _connectionString = _configuration.GetConnectionString("DefaultConnection");
  }


    public bool CreateOrder(Order order)
    {
        bool result = false;
        string created = Helper.FormatDate(order.Created);
        string updated = Helper.FormatDate(order.Updated);

        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            using (MySqlCommand command = new MySqlCommand())
            {
                command.Connection = connection;
                command.Parameters.Clear();

                
                try
                {
                    connection.Open();

                    command.CommandText =
                        "INSERT INTO Orders (Id, UserId, PaymentId, TotalCourse, TotalCost, StatusPayment, Created, Updated) " +
                        "VALUES (@Id, @UserId, @PaymentId, @TotalCourse, @TotalCost, @StatusPayment, @Created, @Updated);";

                    command.Parameters.AddWithValue("@Id", order.Id);
                    command.Parameters.AddWithValue("@UserId", order.UserId);
                    command.Parameters.AddWithValue("@PaymentId", order.PaymentId);
                    command.Parameters.AddWithValue("@TotalCourse", order.TotalCourse);
                    command.Parameters.AddWithValue("@TotalCost", order.TotalCost);
                    command.Parameters.AddWithValue("@StatusPayment", order.StatusPayment);
                    command.Parameters.AddWithValue("@Created", created);
                    command.Parameters.AddWithValue("@Updated", updated);

                    int execResult = command.ExecuteNonQuery();

                    result = execResult > 0 ? true : false;
                }
                catch (Exception ex) 
                {
                    throw ex; 
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        return result;
    }




}