﻿using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class OrderDetailDataAccess
{
  private readonly string _connectionString;
  private readonly IConfiguration _configuration;

  public OrderDetailDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _connectionString = _configuration.GetConnectionString("DefaultConnection");
  }

    public bool AddOrderDetail(OrderDetail orderDetail, Guid userId)
    {
    bool result = false;
    string created = Helper.FormatDate(orderDetail.Created);
    string updated = Helper.FormatDate(orderDetail.Updated);

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.Parameters.Clear();

        string lastinvoiceNumber = orderDetail.LastInvoiceNumber;
        string invoiceNumber = GenerateNewInvoiceNumber(lastinvoiceNumber);

           

        command.CommandText = "INSERT INTO OrderDetails (Id, OrderId, ScheduleId, Created, Updated) " +
                              "VALUES (@Id, @OrderId, @SchedulesId, @Created, @Updated);";

        command.Parameters.AddWithValue("@Id", orderDetail.Id);
        command.Parameters.AddWithValue("@OrderId", orderDetail.OrderId);
        command.Parameters.AddWithValue("@SchedulesId", orderDetail.ScheduleId);
        command.Parameters.AddWithValue("@Created", created);
        command.Parameters.AddWithValue("@Updated", updated);

        try
        {
          connection.Open();

          int execresult = command.ExecuteNonQuery();

                    // Setelah berhasil membuat order, tambahkan entri ke tabel Invoices
                    if (execresult > 0)
                    {
                        string invoiceId = Guid.NewGuid().ToString(); // Buat ID invoice baru

                        // Tambahkan entri ke tabel Invoices dengan nomor invoice yang dihasilkan dan OrderId
                        string insertInvoiceQuery = "INSERT INTO Invoices (Id, OrderDetailsId, UserId, NoInvoice, Created, Updated) " +
                                                    "VALUES (@InvoiceId, @OrderDetailsId, @UserId, @NoInvoice, @Created, @Updated);";

                        using (MySqlCommand insertInvoiceCommand = new MySqlCommand(insertInvoiceQuery, connection))
                        {
                            insertInvoiceCommand.Parameters.AddWithValue("@InvoiceId", invoiceId);
                            insertInvoiceCommand.Parameters.AddWithValue("@OrderDetailsId", orderDetail.Id);
                            insertInvoiceCommand.Parameters.AddWithValue("@UserId", userId);
                            insertInvoiceCommand.Parameters.AddWithValue("@NoInvoice", invoiceNumber);
                            insertInvoiceCommand.Parameters.AddWithValue("@Created", created);
                            insertInvoiceCommand.Parameters.AddWithValue("@Updated", updated);

                            int invoiceResult = insertInvoiceCommand.ExecuteNonQuery();

                            // Jika berhasil membuat invoice, result diubah menjadi true
                            result = invoiceResult > 0;
                        }
                    }
                }
        catch
        {
          throw;
        }
        finally
        {
          connection.Close();
        }
      }
    }

    return result;
  }

    public string GetLastInvoiceNumberByUserId(Guid userId)
    {
        using (var connection = new MySqlConnection(_connectionString))
        {
            connection.Open();

            var getLastInvoiceQuery = "SELECT NoInvoice FROM Invoices WHERE UserId = @UserId ORDER BY Created DESC LIMIT 1";
            using (var getLastInvoiceCommand = new MySqlCommand(getLastInvoiceQuery, connection))
            {
                getLastInvoiceCommand.Parameters.AddWithValue("@UserId", userId);

                var lastInvoice = getLastInvoiceCommand.ExecuteScalar() as string;

                if (string.IsNullOrEmpty(lastInvoice))
                {
                    return "APM000"; // Mengembalikan "APM000" jika nomor faktur tidak ditemukan
                }
                else
                {
                    return lastInvoice;
                }
            }
        }
    }



    public string GenerateNewInvoiceNumber(string lastInvoice)
    {
        if (string.IsNullOrEmpty(lastInvoice))
        {
            return "APM001";
        }
        else
        {
            int lastInvoiceNumber;
            if (int.TryParse(lastInvoice.Substring(3), out lastInvoiceNumber))
            {
                var newInvoiceNumber = lastInvoiceNumber + 1;
                return $"APM{newInvoiceNumber:D3}";
            }
            else
            {
                return "APM001";
            }
        }
    }


}