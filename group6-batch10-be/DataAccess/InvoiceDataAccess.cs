﻿using group6_batch10_be.DTOs.Invoice;
using group6_batch10_be.DTOs.OrderDetail;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class InvoiceDataAccess
{
  private readonly string _connectionString;
  private readonly IConfiguration _configuration;

  public InvoiceDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _connectionString = _configuration.GetConnectionString("DefaultConnection");
  }

    public List<InvoiceDTO> GetOrdersByIdUser(string userid)
    {
        List<InvoiceDTO> invoices = new List<InvoiceDTO>();

        string query =
            "SELECT DISTINCT o.Id AS id_order, i.NoInvoice AS invoice, o.TotalCourse AS total_course, o.TotalCost AS total_price, o.Created AS created_at " +
            "FROM Orders o " +
            "INNER JOIN OrderDetails od ON o.Id = od.OrderId " +
            "INNER JOIN Invoices i ON od.Id = i.OrderDetailsId " +
            "WHERE o.UserId = @id_user " +
            "ORDER BY i.NoInvoice DESC;";

        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            using (MySqlCommand command = new MySqlCommand(query, connection))
            {
                command.Connection = connection;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_user", userid);

                try
                {
                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            invoices.Add(new InvoiceDTO
                            {
                                OrderId = Guid.Parse(reader["id_order"].ToString() ?? string.Empty),
                                NoInvoice = reader["invoice"].ToString() ?? string.Empty,
                                TotalCourse = Convert.ToInt32(reader["total_course"]),
                                TotalCost = Convert.ToDecimal(reader["total_price"]),
                                Created = Convert.ToDateTime(reader["created_at"])
                            });
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        return invoices;
    }




    public InvoiceDTO? GetOrderById(Guid id_order)
    {
        InvoiceDTO? order = null;

        string query =
            "SELECT o.Id AS id_order, i.NoInvoice AS invoice, o.TotalCourse AS total_course, o.TotalCost AS total_price, o.Created AS created_at " +
            "FROM Orders o " +
            "INNER JOIN OrderDetails od ON o.Id = od.OrderId " +
            "INNER JOIN Invoices i ON od.Id = i.OrderDetailsId " +
            "WHERE o.Id = @id_order";

        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            using (MySqlCommand command = new MySqlCommand(query, connection))
            {
                command.Connection = connection;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_order", id_order);

                try
                {
                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            order = new InvoiceDTO
                            {
                                OrderId = Guid.Parse(reader["id_order"].ToString() ?? string.Empty),
                                NoInvoice = reader["invoice"].ToString() ?? string.Empty,
                                TotalCourse = Convert.ToInt32(reader["total_course"]),
                                TotalCost = Convert.ToDecimal(reader["total_price"]),
                                Created = Convert.ToDateTime(reader["created_at"])
                            };
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        return order;
    }


    public List<AllOrderInvoiceDTO> GetAllOrders()
    {
        List<AllOrderInvoiceDTO> invoices = new List<AllOrderInvoiceDTO>();

        string query =
            "SELECT DISTINCT o.Id AS id_order, i.NoInvoice AS invoice, o.TotalCourse AS total_course, o.TotalCost AS total_price, o.Created AS created_at, u.Email " +
            "FROM Orders o " +
            "INNER JOIN OrderDetails od ON o.Id = od.OrderId " +
            "INNER JOIN Invoices i ON od.Id = i.OrderDetailsId " +
            "INNER JOIN Users u ON o.UserId = u.Id " +
            "ORDER BY created_at DESC;";


        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            using (MySqlCommand command = new MySqlCommand(query, connection))
            {
                command.Connection = connection;
                command.Parameters.Clear();

                try
                {
                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            invoices.Add(new AllOrderInvoiceDTO
                            {
                                OrderId = Guid.Parse(reader["id_order"].ToString() ?? string.Empty),
                                Email = reader["Email"].ToString() ?? string.Empty,
                                NoInvoice = reader["invoice"].ToString() ?? string.Empty,
                                TotalCourse = Convert.ToInt32(reader["total_course"]),
                                TotalCost = Convert.ToDecimal(reader["total_price"]),
                                Created = Convert.ToDateTime(reader["created_at"])
                            });
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        return invoices;
    }



    public List<DetailInvoiceDTO> GetDetailOrder(string id_order)
    {
        List<DetailInvoiceDTO> detailInvoice = new List<DetailInvoiceDTO>();

        string query = "SELECT c.Title, ca.Name AS Category, s.Date, c.Price " +
               "FROM Orders o " +
               "INNER JOIN OrderDetails od ON o.Id = od.OrderId " +
               "INNER JOIN Schedules s ON od.ScheduleId = s.Id " +
               "INNER JOIN Courses c ON s.CourseId = c.Id " +
               "INNER JOIN Categories ca ON c.CategoryId = ca.Id " +
               "WHERE o.Id = @id_order " +
               "ORDER BY od.Created DESC";


        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            using (MySqlCommand command = new MySqlCommand(query, connection))
            {
                command.Connection = connection;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_order", id_order);

                try
                {
                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            detailInvoice.Add(new DetailInvoiceDTO
                            {
                                Title = reader["Title"].ToString() ?? string.Empty,
                                Category = reader["Category"].ToString() ?? string.Empty,
                                Date = Convert.ToDateTime(reader["Date"]),
                                Price = Convert.ToDecimal(reader["Price"])
                            });
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        return detailInvoice;
    }



    public List<MyClassDTO> GetMyClass(string id_user)
    {
        List<MyClassDTO> myClass = new List<MyClassDTO>();

        string query = "SELECT i.OrderDetailsId, i.UserId, c.Image, t.Name AS Category, c.Title, s.Date " +
               "FROM Invoices i " +
               "INNER JOIN OrderDetails od ON i.OrderDetailsId = od.Id " +
               "INNER JOIN Orders o ON od.OrderId = o.Id " +
               "INNER JOIN Schedules s ON od.ScheduleId = s.Id " +
               "INNER JOIN Courses c ON s.CourseId = c.Id " +
               "INNER JOIN Categories t ON c.CategoryId = t.Id " +
               "WHERE i.UserId = @id_user " +
               "ORDER BY i.NoInvoice DESC;";  


        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            using (MySqlCommand command = new MySqlCommand(query, connection))
            {
                command.Connection = connection;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_user", id_user);

                try
                {
                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            byte[] imageBytes = (byte[])reader["Image"];
                            string base64Image = Convert.ToBase64String(imageBytes);
                            myClass.Add(new MyClassDTO
                            {
                                //OrderDetailsId = reader["OrderDetailsId"].ToString() ?? string.Empty,
                                //UserId = reader["UserId"].ToString() ?? string.Empty,
                                Category = reader["Category"].ToString() ?? string.Empty,
                                Title = reader["Title"].ToString() ?? string.Empty,
                                Date = Convert.ToDateTime(reader["Date"]),
                                Image = base64Image,
                            });
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        return myClass;
    }







}