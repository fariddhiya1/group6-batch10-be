﻿using group6_batch10_be.DTOs.User;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;
using System.Data;

namespace group6_batch10_be.DataAccess;

public class UserDataAccess
{
  private readonly string _connectionString;
  private readonly IConfiguration _configuration;

  public UserDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _connectionString = _configuration.GetConnectionString("DefaultConnection");
  }


  public bool CreateUserAccount(User user, UserRole userRole)
  {
    bool result = false;
    bool isEmailRegistered = IsEmailAlreadyRegistered(user.Email);

    if (isEmailRegistered)
    {
      return result;
    }

    string created = user.Created.Date.ToString("yyyy-MM-dd HH:mm:ss");
    string updated = user.Updated.Date.ToString("yyyy-MM-dd HH:mm:ss");

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      connection.Open();

      MySqlTransaction transaction = connection.BeginTransaction();

      try
      {
        MySqlCommand command1 = new MySqlCommand();
        command1.Connection = connection;
        command1.Transaction = transaction;
        command1.Parameters.Clear();

        command1.CommandText =
          "INSERT INTO Users (Id, Fullname, Password, Email, IsActivated, Created, Updated)" +
          "VALUES (@id, @fullname, @password, @email, @isActivated, @created, @updated)";
        command1.Parameters.AddWithValue("@id", user.Id);
        command1.Parameters.AddWithValue("@fullname", user.Fullname);
        command1.Parameters.AddWithValue("@password", user.Password);
        command1.Parameters.AddWithValue("@email", user.Email);
        command1.Parameters.AddWithValue("@isActivated", user.IsActivated);
        command1.Parameters.AddWithValue("@created", created);
        command1.Parameters.AddWithValue("@updated", updated);


        MySqlCommand command2 = new MySqlCommand();
        command2.Connection = connection;
        command2.Transaction = transaction;
        command2.Parameters.Clear();

        command2.CommandText =
          "INSERT INTO UserRoles (Id, UserId, Role, Created, Updated) VALUES (@id, @userId, @role, @created, @updated)";
        command2.Parameters.AddWithValue("@id", user.Id);
        command2.Parameters.AddWithValue("@userId", userRole.UserId);
        command2.Parameters.AddWithValue("@role", userRole.Role);
        command2.Parameters.AddWithValue("@created", created);
        command2.Parameters.AddWithValue("@updated", updated);


        var result1 = command1.ExecuteNonQuery();
        var result2 = command2.ExecuteNonQuery();

        transaction.Commit();

        result = true;
      }
      catch (Exception ex)
      {
        transaction.Rollback();
      }
      finally
      {
        connection.Close();
      }
    }

    return result;
  }


  public bool IsEmailAlreadyRegistered(string email)
  {
    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      connection.Open();
      string query = "SELECT COUNT(*) FROM Users WHERE Email = @email";
      MySqlCommand command = new MySqlCommand(query, connection);
      command.Parameters.AddWithValue("@email", email);
      int count = Convert.ToInt32(command.ExecuteScalar());

      return count > 0;
    }
  }

  public UserRole? GetUserRole(Guid userId)
  {
    UserRole? userRole = null;

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.Parameters.Clear();

        command.CommandText = "SELECT * FROM UserRoles WHERE UserId = @userId";
        command.Parameters.AddWithValue("@userId", userId);


        connection.Open();

        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            userRole = new UserRole
            {
              Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
              UserId = Guid.Parse(reader["UserId"].ToString() ?? string.Empty),
              Role = reader.GetBoolean("Role")
            };
          }
        }

        connection.Close();
      }
    }

    return userRole;
  }

  public bool AcitvateUser(Guid id)
  {
    bool result = false;

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      MySqlCommand command = new MySqlCommand();
      command.Connection = connection;
      command.Parameters.Clear();

      command.CommandText = "UPDATE Users SET IsActivated = 1 WHERE Id = @id";
      command.Parameters.AddWithValue("@id", id);

      try
      {
        connection.Open();

        result = command.ExecuteNonQuery() > 0;
      }
      catch
      {
        throw;
      }
      finally
      {
        connection.Close();
      }
    }

    return result;
  }

  public bool ResetPassword(string email, string password)
  {
    bool result = false;

    string query = "UPDATE Users SET Password = @Password, Updated = NOW() WHERE Email = @Email";

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.Parameters.Clear();

        command.CommandText = query;

        command.Parameters.AddWithValue("@Email", email);
        command.Parameters.AddWithValue("@Password", password);

        connection.Open();

        result = command.ExecuteNonQuery() > 0 ? true : false;

        connection.Close();
      }
    }

    return result;
  }


  public User? CheckUser(string email)
  {
    User? user = null;

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.CommandText = "SELECT * From Users WHERE Email = @email";

        command.Parameters.Clear();

        command.Parameters.AddWithValue("@email", email);

        connection.Open();

        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            user = new User
            {
              Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
              Email = reader["Email"].ToString() ?? string.Empty,
              Password = reader["Password"].ToString() ?? string.Empty,
              IsActivated = reader.GetBoolean("IsActivated")
            };
          }
        }

        connection.Close();
      }
    }

    return user;
  }

  public List<UserDTO> GetAllUsers()
  {
    List<UserDTO> users = new List<UserDTO>();

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
     string query = "SELECT u.id AS UserId, u.Fullname, u.Email, u.Password, u.isActivated, r.Role " +
                     "FROM Users u " +
                     "LEFT JOIN UserRoles r ON u.Id = r.UserId " +
                     "ORDER BY u.created DESC;";


            connection.Open();

      using (MySqlCommand command = new MySqlCommand(query, connection))
      using (MySqlDataReader reader = command.ExecuteReader())
      {
        while (reader.Read())
        {
          users.Add(new UserDTO
          {
            Id = Guid.Parse(reader["UserId"].ToString() ?? string.Empty),
            Fullname = reader["Fullname"].ToString(),
            Email = reader["Email"].ToString(),
            Password = reader["Password"].ToString(),
            IsActivated = reader["isActivated"] as bool?,
            Role = reader.GetBoolean("Role")
          });
        }
      }
    }

    return users;
  }


    public bool UpdateUser(string userId, UpdateUserDTO updatedUser)
    {
        using (MySqlConnection connection = new MySqlConnection(_connectionString))
        {
            connection.Open();

            using (MySqlCommand command = new MySqlCommand())
            {
                command.Connection = connection;
                command.CommandType = CommandType.Text;

                command.CommandText =
                    "UPDATE Users SET Fullname = @Fullname, Email = @Email, IsActivated = @IsActivated WHERE Id = @UserId;" +
                    "UPDATE UserRoles SET Role = @Role WHERE UserId = @UserId;";

                command.Parameters.AddWithValue("@UserId", userId);
                command.Parameters.AddWithValue("@Fullname", updatedUser.Fullname);
                command.Parameters.AddWithValue("@Email", updatedUser.Email);
                command.Parameters.AddWithValue("@IsActivated", updatedUser.IsActivated);
                command.Parameters.AddWithValue("@Role", updatedUser.Role);

                int rowsAffected = command.ExecuteNonQuery();

                return rowsAffected > 0;
            }
        }
    }

}