using group6_batch10_be.DTOs;
using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class CourseDataAccess
{
  private readonly string _conectionString;
  private readonly IConfiguration _configuration;

  public CourseDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _conectionString = _configuration.GetConnectionString("DefaultConnection");
  }


  public List<CourseDetailDTO> GetAll()
  {
    List<CourseDetailDTO> courses = new List<CourseDetailDTO>();
    string query =
        "SELECT courses.Id AS CourseId, " +
        "courses.Title, " +
        "courses.Description, " +
        "courses.Image, " +
        "courses.Price, " +
        "courses.IsActive, " +
        "courses.Created, " +
        "courses.Updated, " +
        "category.name " +
        "FROM Courses courses " +
        "INNER JOIN Categories category ON courses.CategoryId = category.Id " +
        "ORDER BY courses.Created DESC;";



        using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        connection.Open();
        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            CourseDetailDTO course = new CourseDetailDTO()
            {
              Id = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty),
              Title = reader["Title"].ToString() ?? string.Empty,
              Description = reader["Description"].ToString() ?? string.Empty,
              Image =  base64Image,
              Price = Convert.ToDecimal(reader["Price"]),
              IsActive = (bool)reader["IsActive"],
              Category = reader["name"].ToString(),
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"])
            };
            courses.Add(course);
          }
        }

        connection.Close();
      }
    }

    return courses;
  }

  public CourseDetailDTO? GetById(Guid id)
  {
    CourseDetailDTO? course = null;

    string query =
      "SELECT courses.Id AS CourseId, " +
      "courses.Title, " +
      "courses.Description, " +
      "courses.Image, " +
      "courses.Price, " +
      "courses.IsActive, " +
      "courses.Created, " +
      "courses.Updated, " +
      "category.name " +
      "FROM Courses courses " +
      "INNER JOIN Categories category ON courses.CategoryId = category.Id " +
      "WHERE courses.Id = @Id";
    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", id);
        connection.Open();
        using (MySqlDataReader reader = command.ExecuteReader())
        {
   
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            course = new CourseDetailDTO
            {
              Id = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty),
              Title = reader["Title"].ToString() ?? string.Empty,
              Description = reader["Description"].ToString() ?? string.Empty,
              Image =  base64Image,
              IsActive = (bool)reader["IsActive"],
              Price = Convert.ToDecimal(reader["Price"]),
              Category = reader["name"].ToString(),
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"])
            };
          }
        }

        connection.Close();
      }
    }

    return course;
  }

  public bool Insert(Course course)
  {
    bool result = false;

    string created = Helper.FormatDate(course.Created);
    string updated = Helper.FormatDate(course.Updated);

    string query =
      "INSERT INTO Courses(Id, Title, Description, Price, IsActive, Image, CategoryId, Created, Updated) " +
      "VALUES (@Id, @Title, @Description, @Price, @IsActive, @Image, @CategoryId, @Created, @Updated)";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", course.Id);
        command.Parameters.AddWithValue("@Title", course.Title);
        command.Parameters.AddWithValue("@Description", course.Description);
        command.Parameters.AddWithValue("@Price", course.Price);
        command.Parameters.AddWithValue("@IsActive", course.IsActive);
        command.Parameters.AddWithValue("@CategoryId", course.CategoryId);
        command.Parameters.AddWithValue("@Created", created);
        command.Parameters.AddWithValue("@Updated", updated);

        if (course.ImageFile.FileName != null)
        {
          byte[] photo = Helper.GetImageBytes(course.ImageFile);  
          command.Parameters.AddWithValue("@Image", photo);
        }

        connection.Open();
        result = command.ExecuteNonQuery() > 0;
        connection.Close();
      }
    }

    return result;
  }


  public bool Update(Guid id, Course course)
{
    bool result = false;

    string query = "UPDATE Courses SET ";
    List<string> setClauses = new List<string>();

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
        using (MySqlCommand command = new MySqlCommand())
        {
            command.Connection = connection;

            if (!string.IsNullOrEmpty(course.Title))
            {
                setClauses.Add("Title = @Title");
                command.Parameters.AddWithValue("@Title", course.Title);
            }

            if (!string.IsNullOrEmpty(course.Description))
            {
                setClauses.Add("Description = @Description");
                command.Parameters.AddWithValue("@Description", course.Description);
            }

            if (course.Price != null)
            {
                setClauses.Add("Price = @Price");
                command.Parameters.AddWithValue("@Price", course.Price);
            }

            if (course.IsActive != null)
            {
                setClauses.Add("IsActive = @IsActive");
                command.Parameters.AddWithValue("@IsActive", course.IsActive);
            }

            if (course.ImageFile != null && course.ImageFile.FileName != null)
            {
                byte[] photo = Helper.GetImageBytes(course.ImageFile);
                setClauses.Add("Image = @Image");
                command.Parameters.AddWithValue("@Image", photo);
            }

            if (setClauses.Count > 0)
            {
                query += string.Join(", ", setClauses);
            }

            query += $" , Updated = NOW() WHERE Id = @Id";
            command.Parameters.AddWithValue("@Id", id);

            command.CommandText = query;

            connection.Open();
            result = command.ExecuteNonQuery() > 0 ? true : false;
        }
    }

    return result;
}

  public bool Delete(Guid id)
  {
    bool result = false;

    string query = $"DELETE FROM Courses WHERE Id = @Id";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", id);

        connection.Open();
        result = command.ExecuteNonQuery() > 0 ? true : false;
        connection.Close();
      }
    }

    return result;
  }

  public List<CourseScheduleDTO> GetAllCourseWithSchedule()
  {
    List<CourseScheduleDTO> courses = new List<CourseScheduleDTO>();

    string queryCourses =
      "SELECT courses.Id AS CourseId, " +
      "courses.Title, " +
      "courses.Description, " +
      "courses.Image AS Image, " +
      "courses.Price, " +
      "courses.IsActive, " +
      "courses.Created, " +
      "courses.Updated, " +
      "category.name " +
      "FROM Courses courses " +
      "INNER JOIN Categories category ON courses.CategoryId = category.Id";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      connection.Open();

      try
      {
        MySqlCommand commandCourses = new MySqlCommand(queryCourses, connection);
        using (MySqlDataReader reader = commandCourses.ExecuteReader())
        {
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            CourseScheduleDTO course = new CourseScheduleDTO()
            {
              Id = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty),
              Title = reader["Title"].ToString(),
              Category = reader["name"].ToString(),
              Price = Convert.ToDecimal(reader["Price"]),
              Image = base64Image,
              IsActive = (bool)reader["IsActive"],
              ListSchedule = new List<ScheduleList>()
            };

            courses.Add(course);
          }
        }

        string querySchedules =
          "SELECT s.CourseId, s.Id, s.Date " +
          "FROM Schedules s";

        MySqlCommand commandSchedules = new MySqlCommand(querySchedules, connection);
        using (MySqlDataReader reader = commandSchedules.ExecuteReader())
        {
          while (reader.Read())
          {
            Guid courseId = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty);
            Guid scheduleId = Guid.Parse(reader["Id"].ToString() ?? string.Empty);
            DateTime scheduleDate = Convert.ToDateTime(reader["Date"]);

            CourseScheduleDTO course = courses.Find(c => c.Id == courseId);

            if (course != null)
            {
              ScheduleList schedule = new ScheduleList()
              {
                Id = scheduleId,
                Date = scheduleDate
              };

              course.ListSchedule.Add(schedule);
            }
          }
        }
      }
      finally
      {
        connection.Close();
      }
    }

    return courses;
  }

    public CourseScheduleDTO GetCourseWithScheduleById(Guid courseId)
    {
        CourseScheduleDTO course = null;

        string queryCourse =
            "SELECT courses.Id AS CourseId, " +
            "courses.Title, " +
            "courses.Description, " +
            "courses.Image, " +
            "courses.Price, " +
            "courses.IsActive, " +
            "courses.Created, " +
            "courses.Updated, " +
            "category.name " +
            "FROM Courses courses " +
            "INNER JOIN Categories category ON courses.CategoryId = category.Id " +
            "WHERE courses.Id = @CourseId";

        using (MySqlConnection connection = new MySqlConnection(_conectionString))
        {
            connection.Open();

            try
            {
                MySqlCommand commandCourse = new MySqlCommand(queryCourse, connection);
                commandCourse.Parameters.AddWithValue("@CourseId", courseId);

                using (MySqlDataReader reader = commandCourse.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        course = new CourseScheduleDTO()
                        {
                            Id = Guid.Parse(reader["CourseId"].ToString() ?? string.Empty),
                            Title = reader["Title"].ToString(),
                            Category = reader["name"].ToString(),
                            Price = Convert.ToDecimal(reader["Price"]),
                            ListSchedule = new List<ScheduleList>()
                        };
                    }
                }

                if (course != null)
                {
                    string querySchedules =
                        "SELECT s.CourseId, s.Id, s.Date " +
                        "FROM Schedules s " +
                        "WHERE s.CourseId = @CourseId";

                    MySqlCommand commandSchedules = new MySqlCommand(querySchedules, connection);
                    commandSchedules.Parameters.AddWithValue("@CourseId", courseId);

                    using (MySqlDataReader reader = commandSchedules.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid scheduleId = Guid.Parse(reader["Id"].ToString() ?? string.Empty);
                            DateTime scheduleDate = Convert.ToDateTime(reader["Date"]);

                            ScheduleList schedule = new ScheduleList()
                            {
                                Id = scheduleId,
                                Date = scheduleDate
                            };

                            course.ListSchedule.Add(schedule);
                        }
                    }
                }
            }
            finally
            {
                connection.Close();
            }
        }

        return course;
    }


}