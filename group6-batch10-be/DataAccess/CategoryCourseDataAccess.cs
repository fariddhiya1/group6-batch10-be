﻿using group6_batch10_be.DTOs.CatergoryCourse;
using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySqlConnector;

namespace group6_batch10_be.DataAccess;

public class CategoryCourseDataAccess
{
  private readonly string _connectionString;
  private readonly IConfiguration _configuration;

  public CategoryCourseDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _connectionString = _configuration.GetConnectionString("DefaultConnection");
  }

  public List<CategoryDetailDTO> GetAll()
  {
    List<CategoryDetailDTO> categories = new List<CategoryDetailDTO>();

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      string query = "SELECT * FROM Categories " +
                     "ORDER BY created DESC;";


            using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        connection.Open();

        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            categories.Add(new CategoryDetailDTO()
            {
              Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
              Name = reader["Name"].ToString() ?? string.Empty,
              Image = base64Image,
              Description = reader["Description"].ToString(),
              IsActive = reader.GetBoolean(reader.GetOrdinal("IsActive")),
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"])
            });
          }
        }
      }
    }

    return categories;
  }


  public CategoryDetailDTO? GetById(Guid id)
  {
    CategoryDetailDTO? categories = null;

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.CommandText = "SELECT * FROM Categories WHERE Id = @id";

        command.Parameters.Clear();

        command.Parameters.AddWithValue("@id", id);

        connection.Open();

        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            categories = new CategoryDetailDTO
            {
              Id = Guid.Parse(reader["id"].ToString() ?? string.Empty),
              Name = reader["Name"].ToString() ?? string.Empty,
              Image = base64Image,
              Description = reader["Description"].ToString(),
              IsActive = reader.GetBoolean(reader.GetOrdinal("IsActive")),
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"])
            };
          }
        }
      }
    }

    return categories;
  }


  public bool Insert(CategoryCourse categories)
  {
    bool result = false;

    string created = categories.Created.Date.ToString("yyyy-MM-dd HH:mm:ss");
    string updated = categories.Updated.Date.ToString("yyyy-MM-dd HH:mm:ss");

    string query = "INSERT INTO Categories (id, Image, Name, Description, IsActive, Created, Updated) " +
                   "VALUES (@id, @image, @name, @description, @isActive, @created, @updated)";


    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.Parameters.Clear();

        command.CommandText = query;
        command.Parameters.AddWithValue("@id", categories.Id);
        command.Parameters.AddWithValue("@name", categories.Name);
        command.Parameters.AddWithValue("@description", categories.Description);
        command.Parameters.AddWithValue("@isactive", categories.IsActive);
        command.Parameters.AddWithValue("@created", created);
        command.Parameters.AddWithValue("@updated", updated);

        if (categories.ImageFile.FileName != null)
        {
          byte[] photo = Helper.GetImageBytes(categories.ImageFile);
          command.Parameters.AddWithValue("@image", photo);
        }
        else
        {
          command.Parameters.AddWithValue("@image", DBNull.Value);
        }

        connection.Open();

        result = command.ExecuteNonQuery() > 0 ? true : false;

        connection.Close();
      }
    }

    return result;
  }


  public bool Update(Guid id, CategoryCourse categories)
  {
    bool result = false;

    // Initialize an empty list to store the SET clauses
    List<string> setClauses = new List<string>();

    if (!string.IsNullOrEmpty(categories.Name))
    {
      setClauses.Add("Name = @name");
    }

    if (!string.IsNullOrEmpty(categories.Description))
    {
      setClauses.Add("Description = @description");
    }

    if (categories.IsActive != null)
    {
      setClauses.Add("isActive = @isActive");
    }

    byte[] photo = null;
    
    if (categories.ImageFile != null && categories.ImageFile.FileName != null)
    {
      photo = Helper.GetImageBytes(categories.ImageFile);
      setClauses.Add("Image = @image");
    }
    
    // Construct the SQL query dynamically based on provided parameters
    string setClause = string.Join(", ", setClauses);

    if (!string.IsNullOrEmpty(setClause))
    {
      string query = $"UPDATE Categories SET {setClause}, Updated = NOW() WHERE Id = @id";

      using (MySqlConnection connection = new MySqlConnection(_connectionString))
      {
        using (MySqlCommand command = new MySqlCommand(query, connection))
        {
          command.Parameters.AddWithValue("@id", id);

          if (!string.IsNullOrEmpty(categories.Name))
          {
            command.Parameters.AddWithValue("@name", categories.Name);
          }

          if (!string.IsNullOrEmpty(categories.Description))
          {
            command.Parameters.AddWithValue("@description", categories.Description);
          }

          if (categories.IsActive != null)
          {
            command.Parameters.AddWithValue("@isActive", categories.IsActive);
          }

          if (photo != null)
          {
            command.Parameters.AddWithValue("@image", photo);
          }

          connection.Open();
          result = command.ExecuteNonQuery() > 0;
          connection.Close();
        }
      }
    }
    else
    {
      // No valid parameters to update, so consider it a success
      result = true;
    }

    return result;
  }


  public bool Delete(Guid id)
  {
    bool result = false;

    string query = $"DELETE FROM Categories WHERE Id = '{id}'";

    using (MySqlConnection connection = new MySqlConnection(_connectionString))
    {
      using (MySqlCommand command = new MySqlCommand())
      {
        command.Connection = connection;
        command.CommandText = query;

        connection.Open();

        result = command.ExecuteNonQuery() > 0 ? true : false;

        connection.Close();
      }
    }

    return result;
  }
}