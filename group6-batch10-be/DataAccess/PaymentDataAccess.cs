using group6_batch10_be.DTOs.Payment;
using group6_batch10_be.Helpers;
using group6_batch10_be.Models;
using MySql.Data.MySqlClient;

namespace group6_batch10_be.DataAccess;

public class PaymentDataAccess
{
  private readonly string _conectionString;
  private readonly IConfiguration _configuration;

  public PaymentDataAccess(IConfiguration configuration)
  {
    _configuration = configuration;
    _conectionString = _configuration.GetConnectionString("DefaultConnection");
  }

  public List<PaymentDetailDTO> GetAll()
  {
    List<PaymentDetailDTO> payments = new List<PaymentDetailDTO>();
    string query = "SELECT * FROM Payments " +
                   "ORDER BY created DESC;";


        using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        connection.Open();

        using (MySqlDataReader reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            payments.Add(new PaymentDetailDTO
              {
                Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                Method = reader["Method"].ToString(),
                Image = base64Image,
                IsActive = (bool)reader["IsActive"],
                Created = Convert.ToDateTime(reader["Created"]),
                Updated = Convert.ToDateTime(reader["Updated"])
              }
            );
          }
        }

        connection.Close();
      }
    }

    return payments;
  }

  public PaymentDetailDTO? GetById(Guid id)
  {
    PaymentDetailDTO? payment = null;

    string query = $"SELECT * FROM Payments WHERE Id = @Id";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        using (MySqlDataReader reader = command.ExecuteReader())
        {
          command.Parameters.AddWithValue("@Id", id);
          connection.Open();
          while (reader.Read())
          {
            byte[] imageBytes = (byte[])reader["Image"];
            string base64Image = Convert.ToBase64String(imageBytes);
            payment = new PaymentDetailDTO
            {
              Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
              Method = reader["Method"].ToString() ?? string.Empty,
              Image = base64Image,
              IsActive = (bool)reader["IsActive"],
              Created = Convert.ToDateTime(reader["Created"]),
              Updated = Convert.ToDateTime(reader["Updated"])
            };
          }
        }

        connection.Close();
      }
    }

    return payment;
  }

  public bool Insert(Payment payment)
  {
    bool result = false;

    string created = payment.Created.ToString("yyyy-MM-dd HH:mm:ss");
    string updated = payment.Updated.ToString("yyyy-MM-dd HH:mm:ss");

    string query = "INSERT INTO Payments (Id, Method, Image, IsActive,  Created, Updated) " +
                   "VALUES (@Id, @Method, @Image, @IsActive,@Created, @Updated)";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", payment.Id);
        command.Parameters.AddWithValue("@Method", payment.Method);
        command.Parameters.AddWithValue("@IsActive", payment.IsActive);
        command.Parameters.AddWithValue("@Created", created);
        command.Parameters.AddWithValue("@Updated", updated);

        if (payment.ImageFile != null)
        {
          byte[] photo = Helper.GetImageBytes(payment.ImageFile);
          command.Parameters.AddWithValue("@Image", photo);
        }

        connection.Open();
        result = command.ExecuteNonQuery() > 0 ? true : false;
        connection.Close();
      }
    }

    return result;
  }

  public bool Update(Guid id, Payment payment)
  {
    try
    {
      using (MySqlConnection connection = new MySqlConnection(_conectionString))
      {
        connection.Open();

        List<string> setClauses = new List<string>();
        MySqlCommand command = connection.CreateCommand();

        if (!string.IsNullOrEmpty(payment.Method))
        {
          setClauses.Add("Method = @method");
          command.Parameters.AddWithValue("@method", payment.Method);
        }

        if (payment.IsActive != null)
        {
          setClauses.Add("IsActive = @isActive");
          command.Parameters.AddWithValue("@isActive", payment.IsActive);
        }

        if (payment.ImageFile != null && payment.ImageFile.FileName != null)
        {
          byte[] photo = Helper.GetImageBytes(payment.ImageFile);
          setClauses.Add("Image = @image");
          command.Parameters.AddWithValue("@image", photo);
        }

        command.CommandText = $"UPDATE Payments SET {string.Join(", ", setClauses)}, Updated = NOW() WHERE Id = @id";
        command.Parameters.AddWithValue("@id", id);

        int rowsAffected = command.ExecuteNonQuery();

        return rowsAffected > 0;
      }
    }
    catch (Exception ex)
    {
      // Handle exceptions here, e.g., log the error
      return false;
    }
  }

  public bool Delete(Guid id)
  {
    bool result = false;

    string query = $"DELETE FROM Payments WHERE Id = @Id";

    using (MySqlConnection connection = new MySqlConnection(_conectionString))
    {
      using (MySqlCommand command = new MySqlCommand(query, connection))
      {
        command.Parameters.AddWithValue("@Id", id);
        connection.Open();
        result = command.ExecuteNonQuery() > 0 ? true : false;
        connection.Close();
      }
    }

    return result;
  }
}