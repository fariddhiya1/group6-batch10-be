namespace group6_batch10_be.Models;

public class Order
{
  public Guid Id { get; set; }
  public Guid UserId { get; set; }
  public Guid PaymentId { get; set; }
  public int TotalCourse { get; set; }
  public decimal TotalCost { get; set; }
  public string StatusPayment { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}