﻿namespace group6_batch10_be.Models;

public class CategoryCourse
{
  public Guid Id { get; set; }
  public string Name { get; set; } = string.Empty;
  public string? Description { get; set; }
  public bool? IsActive { get; set; }
  public byte? Image { get; set; }
  public IFormFile? ImageFile { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}