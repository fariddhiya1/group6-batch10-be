﻿namespace group6_batch10_be.Models
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public string NoInvoice { get; set; }
        public Guid? OrderId { get; set; }
        public Guid? UserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

    }
}
