﻿namespace group6_batch10_be.Models;

public class Cart
{
  public Guid Id { get; set; }
  public Guid UserId { get; set; }
  public Guid ScheduleId { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}