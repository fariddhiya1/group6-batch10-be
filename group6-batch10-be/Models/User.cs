﻿namespace group6_batch10_be.Models;

    public class User
    {
        public Guid Id { get; set; }
        public string Fullname { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string? Email { get; set; }
        public bool? IsActivated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }


}

