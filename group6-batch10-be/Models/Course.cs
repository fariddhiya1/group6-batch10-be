namespace group6_batch10_be.Models;

public class Course
{
  public Guid Id { get; set; }
  public string Title { get; set; } 
  public string Description { get; set; } 
  public decimal? Price { get; set; }
  public byte? Image { get; set; }
  public IFormFile? ImageFile { get; set; }
  public bool? IsActive { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
  public Guid? CategoryId { get; set; }
}