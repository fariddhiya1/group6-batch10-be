namespace group6_batch10_be.Models;

public class Schedule
{
  public Guid Id { get; set; }
  public DateTime Date { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
  public Guid? CourseId { get; set; } 
}