﻿namespace group6_batch10_be.Models;

    public class UserRole
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public bool Role { get; set; }
    }

