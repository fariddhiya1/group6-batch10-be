namespace group6_batch10_be.Models;

public class Payment
{
  public Guid Id { get; set; }
  public string? Method { get; set; }
  public byte? Image { get; set; }
  public IFormFile? ImageFile { get; set; }
  public bool? IsActive { get; set; }
  public DateTime Created { get; set; }
  public DateTime Updated { get; set; }
}