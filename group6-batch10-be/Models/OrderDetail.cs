﻿namespace group6_batch10_be.Models
{
    public class OrderDetail
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid ScheduleId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public String LastInvoiceNumber { get; set; }    
    }

}
