﻿using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.Order;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrderController : ControllerBase
{
  private readonly OrderDataAccess _orderDataAccess;

  public OrderController(OrderDataAccess orderDataAccess)
  {
    _orderDataAccess = orderDataAccess;
  }

  [HttpPost("CreateOrder")]
  public IActionResult CreateOrder([FromBody] OrderDTO orderDTO)
  {
    try
    {
      var userIdClaim = User.FindFirst("Id");
      if (userIdClaim == null)
      {
        return Unauthorized();
      }

      Guid userId = Guid.Parse(userIdClaim.Value);

      Order orders = new Order
      {
        Id = Guid.NewGuid(),
        UserId = userId,
        PaymentId = orderDTO.PaymentId,
        TotalCourse = orderDTO.TotalCourse,
        TotalCost = orderDTO.TotalCost,
        StatusPayment = "PAID",
        Created = DateTime.UtcNow,
        Updated = DateTime.UtcNow,
      };

      bool result = _orderDataAccess.CreateOrder(orders);

      if (result)
      {
        return StatusCode(201, orders);
      }
      else
      {
        return StatusCode(500, "Data not inserted");
      }
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }


  }

    




}