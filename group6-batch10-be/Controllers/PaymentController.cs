using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.Payment;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PaymentController : ControllerBase
{
  private readonly PaymentDataAccess _paymentDataAccess;

  public PaymentController(PaymentDataAccess paymentDataAccess)
  {
    _paymentDataAccess = paymentDataAccess;
  }

  [HttpGet]
  public IActionResult GetAll()
  {
    try
    {
      var course = _paymentDataAccess.GetAll();
      return Ok(course);
    }
    catch (Exception e)
    {
      return Problem(e.Message);
    }
  }

  [HttpGet("GetById")]
  public IActionResult Get(Guid id)
  {
    try
    {
      var payment = _paymentDataAccess.GetById(id);

      if (payment == null)
      {
        return NotFound("Data Not Found");
      }

      return Ok(payment);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpPost]
  public IActionResult Post([FromForm] PaymentDTO paymentDto)
  {
    try
    {
      if (paymentDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      Payment payment = new Payment
      {
        Id = Guid.NewGuid(),
        Method = paymentDto.Method,
        ImageFile = paymentDto.ImageFile,
        IsActive = paymentDto.IsActive,
        Created = DateTime.Now,
        Updated = DateTime.Now,
      };
      bool result = _paymentDataAccess.Insert(payment);
      return result ? StatusCode(201, payment.Id) : StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpPut("{id}")]
  public IActionResult Put(Guid id, [FromForm] PaymentDTO paymentDto)
  {
    try
    {
      if (paymentDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      Payment payment = new Payment
      {
        Method = paymentDto.Method,
        ImageFile = paymentDto.ImageFile,
        IsActive = paymentDto.IsActive,
        Updated = DateTime.Now
      };

      bool result = _paymentDataAccess.Update(id, payment);
      if (result)
      {
        return NoContent();
      }

      return StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpDelete("{id}")]
  public IActionResult Delete(Guid id)
  {
    try
    {
      bool result = _paymentDataAccess.Delete(id);
      if (!result)
      {
        return StatusCode(500, "Error occurred");
      }

      return NoContent();
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
}