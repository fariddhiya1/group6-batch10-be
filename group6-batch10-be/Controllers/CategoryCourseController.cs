﻿using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.CatergoryCourse;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CategoryCourseController : ControllerBase
{
  private readonly CategoryCourseDataAccess _categoriesDataAccess;

  public CategoryCourseController(CategoryCourseDataAccess categoriesDataAccess)
  {
    _categoriesDataAccess = categoriesDataAccess;
  }

  [HttpGet]
  public IActionResult GetAll()
  {
    try
    {
      var courses = _categoriesDataAccess.GetAll();
      return Ok(courses);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpGet("GetById")]
  public IActionResult Get(Guid id)
  {
    try
    {
      var categories = _categoriesDataAccess.GetById(id);

      if (categories == null)
      {
        return NotFound("Data not found");
      }

      return Ok(categories);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpPost]
  public IActionResult Post([FromForm] CategoryCourseDTO categoriesDto)
  {
    try
    {
      if (categoriesDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      CategoryCourse categories = new CategoryCourse
      {
        Id = Guid.NewGuid(),
        Name = categoriesDto.Name,
        ImageFile = categoriesDto.ImageFile,
        Description = categoriesDto.Description,
        IsActive = categoriesDto.IsActive,
        Created = DateTime.Now,
        Updated = DateTime.Now,
      };

      bool result = _categoriesDataAccess.Insert(categories);

      return result ? StatusCode(201, categories.Id) : StatusCode(500, "Error occurred");
    }

    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
  
  [HttpPut("Update")]
  public IActionResult Put(Guid id, [FromForm] CategoryCourseDTO categoriesDto)
  {
    try
    {
      if (categoriesDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      CategoryCourse course = new CategoryCourse
      {
        Name = categoriesDto.Name,
        ImageFile = categoriesDto.ImageFile,
        Description = categoriesDto.Description,
        IsActive = categoriesDto.IsActive,
        Updated = DateTime.Now
      };

      bool result = _categoriesDataAccess.Update(id, course);

      if (result)
      {
        return NoContent();
      }

      return StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
  
  [HttpDelete("Delete")]
  public IActionResult Delete(Guid id)
  {
    try
    {
      bool result = _categoriesDataAccess.Delete(id);

      if (result)
      {
        return Ok("Data deleted successfully");
      }

      return NoContent();
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
}