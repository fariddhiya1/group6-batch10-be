﻿using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.User;
using group6_batch10_be.Emails;
using group6_batch10_be.Emails.Template;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
  private readonly UserDataAccess _userData;
  private readonly IConfiguration _configuration;
  private readonly EmailService _emailService;

  public UserController(UserDataAccess userData, IConfiguration configuration, EmailService emailService)
  {
    _userData = userData;
    _configuration = configuration;
    _emailService = emailService;
  }

  [HttpPost("Register")]
  public async Task<IActionResult> Register([FromBody] UserDTO userDto)
  {
    try
    {
      User user = new User
      {
        Id = Guid.NewGuid(),
        Fullname = userDto.Fullname,
        Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password),
        Email = userDto.Email,
        IsActivated = false,
        Created = DateTime.Now,
        Updated = DateTime.Now
      };

      UserRole userRole = new UserRole
      {
        UserId = user.Id,
        Role = userDto.Role
      };

      bool isEmailRegistered = _userData.IsEmailAlreadyRegistered(user.Email);

      if (isEmailRegistered)
      {
        return BadRequest(new { message = "Email is already registered." });
      }

      bool result = _userData.CreateUserAccount(user, userRole);

      if (result)
      {
        bool mailResult = await SendMailActivation(user);
        return Ok(new { message = "Registration successful", userDto });
      }
      else
      {
        return BadRequest(new { message = "Registration failed" });
      }
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }

  [HttpPost("login")]
  public IActionResult Login([FromBody] LoginRequestDTO credential)
  {
    if (credential is null)
      return BadRequest(new { message = "Invalid client request" });

    if (string.IsNullOrEmpty(credential.Email) || string.IsNullOrEmpty(credential.Password))
      return BadRequest(new { message = "Invalid client request" });

    User? user = _userData.CheckUser(credential.Email);

    if (user == null)
      return Unauthorized(new { message = "Your account has not been registered" });

    if (user.IsActivated != true)
    {
      return Unauthorized(new { message = "Your account has not activated" });
    }

    UserRole? userRole = _userData.GetUserRole(user.Id);


    bool isVerified = BCrypt.Net.BCrypt.Verify(credential.Password, user.Password);

    if (user != null && !isVerified)
    {
      return BadRequest(new { message = "Incorrect Password!" });
    }
    else
    {
      var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
        _configuration.GetSection("JwtConfig:Key").Value));

      var claims = new Claim[]
      {
        new Claim(ClaimTypes.Email, user.Email),
        new Claim(ClaimTypes.Role, userRole.Role.ToString()),
        new Claim("Id", user.Id.ToString())
      };

      var signingCredential = new SigningCredentials(
        key, SecurityAlgorithms.HmacSha256Signature);

      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(claims),
        Expires = DateTime.UtcNow.AddMinutes(1200),
        SigningCredentials = signingCredential
      };

      var tokenHandler = new JwtSecurityTokenHandler();

      var securityToken = tokenHandler.CreateToken(tokenDescriptor);

      string token = tokenHandler.WriteToken(securityToken);

      return Ok(new LoginResponseDTO { Token = token, Message = "Login Success" });
    }
  }

  [HttpGet("ActivateUser")]
  public IActionResult ActivateUser(Guid userId, string email)
  {
    try
    {
      User? user = _userData.CheckUser(email);

      if (user == null)
        return BadRequest("Activation Failed");

      if (user.IsActivated == true)
        return BadRequest("Account has been activated");

      bool result = _userData.AcitvateUser(userId);

      if (result)
        {
        var emailConfirmUrl = _configuration["EmailConfirmUrl"];
        return Redirect(emailConfirmUrl);
        }
      else
        return BadRequest("Activation Failed");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpPost("SendMailUser")]
  public async Task<IActionResult> SendMailUser([FromBody] string mailTo)
  {
    List<string> to = new List<string>();
    to.Add(mailTo);

    string subject = "Test Email FS10";
    string body = "Hallo, First Email";

    EmailModel model = new EmailModel(to, subject, body);

    bool sendMail = await _emailService.SendAsync(model, new CancellationToken());

    if (sendMail)
      return Ok("Send");
    else
      return StatusCode(500, "Error");
  }


  private async Task<bool> SendMailActivation(User user)
  {
    if (user == null)
      return false;

    if (string.IsNullOrEmpty(user.Email))
      return false;

    List<string> to = new List<string>();
    to.Add(user.Email);

    string subject = "Account Activation";

    var param = new Dictionary<string, string>()
    {
      { "userId", user.Id.ToString() },
      { "email", user.Email }
    };

    var activateUserUrl = _configuration["ActivateUserUrl"];

    string callback = QueryHelpers.AddQueryString(activateUserUrl, param);

    //string body = "Please confirm account by clicking this <a href=\"" + callback + "\"> Link</a>";

    string body = _emailService.GetMailTemplate("EmailActivation", new ActivationModel()
    {
      Email = user.Email,
      Link = callback
    });

    EmailModel emailModel = new EmailModel(to, subject, body);
    bool mailResult = await _emailService.SendAsync(emailModel, new CancellationToken());

    return mailResult;
  }

  [HttpPost("ForgetPassword")]
  public async Task<IActionResult> ForgetPassword(string email)
  {
    try
    {
      if (string.IsNullOrEmpty(email))
        return BadRequest("Email is empty");

      bool isEmailRegistered = _userData.IsEmailAlreadyRegistered(email);

      if (!isEmailRegistered)
      {
        return BadRequest(new { message = "Email is not registered." });
      }

      bool sendMail = await SendEmailForgetPassword(email);

      if (sendMail)
      {
        return Ok(new { message = "A password reset email has been sent to your email" });
      }
      else
      {
        return StatusCode(500, "Error");
      }
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpPost("ResetPassword")]
  public IActionResult ResetPassword([FromBody] ResetPasswordDTO resetPassword)
  {
    try
    {
      if (resetPassword == null)
        return BadRequest("No Data");

      if (resetPassword.Password != resetPassword.ConfirmPassword)
      {
        return BadRequest("Password doesn't match");
      }

      bool reset = _userData.ResetPassword(resetPassword.Email, BCrypt.Net.BCrypt.HashPassword(resetPassword.Password));

      if (reset)
      {
        return Ok("Reset password OK");
      }
      else
      {
        return StatusCode(500, "Error");
      }
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
    

    private async Task<bool> SendEmailForgetPassword(string email)
    {
        // send email
        List<string> to = new List<string>();
        to.Add(email);

        string subject = "Forget Password";

        var param = new Dictionary<string, string?>
                    {
                        {"email", email }
                    };

        var resetUrl = _configuration["ResetPasswordUrl"];

        string callbackUrl = QueryHelpers.AddQueryString(resetUrl, param);

        string body = "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>";

        EmailModel mailModel = new EmailModel(to, subject, body);

        bool mailResult = await _emailService.SendAsync(mailModel, new CancellationToken());

        return mailResult;

    }



    [Authorize]
  [HttpGet("GetAllUsers")]
  public ActionResult<List<UserDTO>> GetAllUsers()
  {
    try
    {
      List<UserDTO> users = _userData.GetAllUsers();

      if (users.Count > 0)
      {
        return Ok(users);
      }
      else
      {
        return NotFound("No users found.");
      }
    }
    catch (Exception ex)
    {
      return StatusCode(500, $"Error: {ex.Message}");
    }
  }

  [Authorize]
  [HttpPut("Update")]
  public IActionResult UpdateUser(string userId, [FromBody] UpdateUserDTO updatedUser)
  {
    try
    {
      bool result = _userData.UpdateUser(userId, updatedUser);

      if (result)
      {
        return Ok(new { message = "User data updated successfully" });
      }
      else
      {
        return NotFound(new { message = "User data update failed" });
      }
    }
    catch (Exception ex)
    {
      return StatusCode(500, $"Error: {ex.Message}");
    }
  }
}