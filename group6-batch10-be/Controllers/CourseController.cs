using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CourseController : ControllerBase
{
  private readonly CourseDataAccess _courseDataAccess;

  public CourseController(CourseDataAccess courseDataAccess, IConfiguration configuration)
  {
    _courseDataAccess = courseDataAccess;
  }

  [HttpGet]
  public IActionResult GetAll()
  {
    try
    {
      var course = _courseDataAccess.GetAll();
      return Ok(course);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpGet("WithSchedule")]
  public IActionResult GetAllCourseWithSchedule()
  {
    try
    {
      var course = _courseDataAccess.GetAllCourseWithSchedule();
      return Ok(course);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

    [HttpGet("WithScheduleId")]
    public IActionResult GetCourseById(Guid courseId)
    {
        try
        {
            var course = _courseDataAccess.GetCourseWithScheduleById(courseId);

            if (course != null)
            {
                return Ok(course);
            }
            else
            {
                return NotFound($"Course with ID {courseId} not found.");
            }
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }


    [HttpGet("GetById")]
  public IActionResult Get(Guid id)
  {
    try
    {
      CourseDetailDTO? course = _courseDataAccess.GetById(id);

      if (course == null)
      {
        return NotFound("Data Not Found");
      }

      return Ok(course);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
  
  [HttpPost]
  public IActionResult Post([FromForm] CourseDto courseDto)
  {
    try
    {
      if (courseDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      Course course = new Course
      {
        Id = Guid.NewGuid(),
        Title = courseDto.Title,
        Description = courseDto.Description,
        ImageFile = courseDto.ImageFile,
        IsActive = courseDto.IsActive,
        Price = courseDto.Price,
        CategoryId = courseDto.CategoryId,
        Created = DateTime.Now,
        Updated = DateTime.Now,
      };

      bool result = _courseDataAccess.Insert(course);
      return result ? StatusCode(201, course.Id) : StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
  
  [HttpPut("{id}")]
  public IActionResult Put(Guid id, [FromForm] CourseDto courseDto)
  {
    try
    {
      if (courseDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      Course course = new Course()
      {
        Title = courseDto.Title,
        Description = courseDto.Description,
        ImageFile = courseDto.ImageFile,
        Price = courseDto.Price,
        IsActive = courseDto.IsActive,
        Updated = DateTime.Now
      };

      bool result = _courseDataAccess.Update(id, course);
      if (result)
      {
        return NoContent();
      }

      return StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
  
  [HttpDelete("{id}")]
  public IActionResult Delete(Guid id)
  {
    try
    {
      bool result = _courseDataAccess.Delete(id);
      if (!result)
      {
        return StatusCode(500, "Error occurred");
      }

      return NoContent();
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
}