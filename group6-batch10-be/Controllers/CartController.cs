﻿using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.Cart;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CartController : ControllerBase
{
  private readonly CartDataAccess _cartDataAccess;

  public CartController(CartDataAccess cartDataAccess)
  {
    _cartDataAccess = cartDataAccess;
  }
  
  [HttpPost("AddCart")]
  public IActionResult AddCart([FromBody] CartRequestDTO cartrequestDTO)
  {
    try
    {
      var userIdClaim = User.FindFirst("Id");

      if (userIdClaim != null && Guid.TryParse(userIdClaim.Value, out Guid userId))
      {
        Cart cart = new Cart
        {
          Id = Guid.NewGuid(),
          UserId = userId,
          ScheduleId = cartrequestDTO.ScheduleId,
          Created = DateTime.Now,
          Updated = DateTime.Now,
        };

        bool result = _cartDataAccess.AddCart(cart);

        if (result)
        {
          return StatusCode(201, cartrequestDTO);
        }
        else
        {
          return StatusCode(500, "Data not inserted");
        }
      }
      else
      {
        return StatusCode(400, "Invalid UserId");
      }
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }


  [HttpGet("GetCartByIdUser")]
  public IActionResult GetCartByIdUser()
  {
    try
    {
      var userIdClaim = User.FindFirst("Id");

      if (userIdClaim != null)
      {
        string userId = userIdClaim.Value;
        var cartList = _cartDataAccess.GetViewCart(userId);

        return Ok(cartList);
      }
      else
      {
        return StatusCode(401, "User ID claim not found");
      }
    }
    catch (Exception ex)
    {
      return StatusCode(500, "An Error Occurs");
    }
  }


  [HttpDelete("DeleteCart")]
  public IActionResult DeleteCart(Guid userId)
  {
    try
    {
      bool result = _cartDataAccess.DeleteCart(userId);

      if (result)
      {
        return NoContent();
      }
      else
      {
        return StatusCode(500, "Error occur");
      }
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }
}