using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ScheduleController : ControllerBase
{
  private readonly ScheduleDataAccess _scheduleDataAccess;

  public ScheduleController(ScheduleDataAccess scheduleDataAccess)
  {
    _scheduleDataAccess = scheduleDataAccess;
  }

  [HttpGet]
  public IActionResult GetAll()
  {
    try
    {
      var schedule = _scheduleDataAccess.GetAll();
      return Ok(schedule);
    }
    catch (Exception e)
    {
      return Problem(e.Message);
    }
  }

  [HttpGet("GetById")]
  public IActionResult Get(Guid id)
  {
    try
    {
      Schedule? schedule = _scheduleDataAccess.GetById(id);

      if (schedule == null)
      {
        return NotFound("Data Not Found");
      }

      return Ok(schedule);
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }


  [HttpPost]
  public IActionResult Post([FromBody] ScheduleDTO scheduleDto)
  {
    try
    {
      if (scheduleDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      Schedule schedule = new Schedule
      {
        Id = Guid.NewGuid(),
        Date = scheduleDto.Date,
        Created = DateTime.Now,
        Updated = DateTime.Now,
        CourseId = scheduleDto.CourseId,
      };
      bool result = _scheduleDataAccess.Insert(schedule);
      return result ? StatusCode(201, schedule.Id) : StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpPut("Update")]
  public IActionResult Put(Guid id, [FromBody] ScheduleDTO scheduleDto)
  {
    try
    {
      if (scheduleDto == null)
      {
        return BadRequest("Data should be inputted");
      }

      Schedule schedule = new Schedule
      {
        Date = scheduleDto.Date,
        Created = DateTime.Now,
      };

      bool result = _scheduleDataAccess.Update(id, schedule);
      if (result)
      {
        return NoContent();
      }

      return StatusCode(500, "Error occurred");
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }

  [HttpDelete]
  public IActionResult Delete(Guid id)
  {
    try
    {
      bool result = _scheduleDataAccess.Delete(id);
      if (!result)
      {
        return StatusCode(500, "Error occurred");
      }

      return NoContent();
    }
    catch (Exception ex)
    {
      return StatusCode(500, ex.Message);
    }
  }
}