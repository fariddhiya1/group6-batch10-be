﻿using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.OrderDetail;
using group6_batch10_be.Models;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrderDetailController : ControllerBase
{
  private readonly OrderDetailDataAccess _orderDetailDataAccess;

  public OrderDetailController(OrderDetailDataAccess orderDetailDataAccess)
  {
    _orderDetailDataAccess = orderDetailDataAccess;
  }


    [HttpPost("AddOrderDetail")]
    public IActionResult AddOrderDetail([FromBody] OrderDetailDTO orderDetailDTO)
    {
        var userIdClaim = User.FindFirst("Id");
        if (userIdClaim == null)
        {
            return Unauthorized();
        }

        Guid userId = Guid.Parse(userIdClaim.Value);

        try
        {
            OrderDetail detail = new OrderDetail
            {
                Id = Guid.NewGuid(),
                OrderId = orderDetailDTO.OrderId,
                ScheduleId = orderDetailDTO.ScheduleId,
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                LastInvoiceNumber = orderDetailDTO.LastInvoiceNumber,
            };

            bool result = _orderDetailDataAccess.AddOrderDetail(detail, userId);

            if (result)
            {
                return StatusCode(201, orderDetailDTO);
            }
            else
            {
                return StatusCode(500, "Data not inserted");
            }
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }


    [HttpGet("LastInvoiceNumber")]
    public IActionResult GetLastInvoiceNumberByUserId()
    {
        var userIdClaim = User.FindFirst("Id");
        if (userIdClaim == null)
        {
            return Unauthorized();
        }

        Guid userId = Guid.Parse(userIdClaim.Value);

        try
        {
            var lastInvoiceNumber = _orderDetailDataAccess.GetLastInvoiceNumberByUserId(userId);

            if (lastInvoiceNumber != null)
            {
                return Ok(lastInvoiceNumber);
            }
            else
            {
                return Ok("APM000"); 
            }
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }
}