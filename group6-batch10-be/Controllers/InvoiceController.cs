﻿using group6_batch10_be.DataAccess;
using group6_batch10_be.DTOs.Invoice;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace group6_batch10_be.Controllers;

[Route("api/[controller]")]
[ApiController]
public class InvoiceController : ControllerBase
{
  private readonly InvoiceDataAccess _invoiceData;

  public InvoiceController(InvoiceDataAccess invoiceDataAccess)
  {
    _invoiceData = invoiceDataAccess;
  }


  [Authorize]
  [HttpGet("GetInvoiceByIdUser")]
  public IActionResult GetOrdersByIdUser()
  {
    try
    {
      var userIdClaim = User.FindFirst("Id");

      if (userIdClaim == null)
      {
        return Unauthorized();
      }

      string id_user = userIdClaim.Value;
      var listOrder = _invoiceData.GetOrdersByIdUser(id_user);

      if (listOrder.Count == 0)
      {
        return NotFound("Data not found");
      }

      return Ok(listOrder);
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }


  [HttpGet("GetInvoiceById")]
  public IActionResult GetOrderById(Guid id_order)
  {
    try
    {
      InvoiceDTO? order = _invoiceData.GetOrderById(id_order);

      if (order == null)
      {
        return NotFound("Data not found");
      }

      return Ok(order);
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }
  
  [HttpGet("GetAllInvoice")]
  public IActionResult GetAllOrders()
  {
    try
    {
      var listOrder = _invoiceData.GetAllOrders();

      if (listOrder.Count == 0)
      {
        return NotFound("Data not found");
      }

      return Ok(listOrder);
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }

  [HttpGet("GetInvoiceDetail")]
  public IActionResult GetDetailOrder(string id_order)
  {
    try
    {
      var details = _invoiceData.GetDetailOrder(id_order);

      if (details.Count == 0)
      {
        return NotFound("Data not found");
      }

      return Ok(details);
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }

  [HttpGet("GetMyClass")]
  public IActionResult GetMyClass()
  {
    try
    {
      var userIdClaim = User.FindFirst("Id");

      if (userIdClaim == null)
      {
        return Unauthorized();
      }

      string id_user = userIdClaim.Value;
      var classList = _invoiceData.GetMyClass(id_user);

      if (classList.Count == 0)
      {
        return NotFound("Data not found");
      }

      return Ok(classList);
    }
    catch (Exception ex)
    {
      return Problem(ex.Message);
    }
  }
}